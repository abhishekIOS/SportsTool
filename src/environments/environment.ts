// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  hmr: false,
  firebase: {
    apiKey: 'AIzaSyBSWd4ks_acpUgfPdzDcwZdB3cxRPOVaqo',
    authDomain: 'sportstools2.firebaseapp.com',
    databaseURL: 'https://sportstools2.firebaseio.com',
    projectId: 'sportstools2',
    storageBucket: 'sportstools2.appspot.com',
    messagingSenderId: '1087131603117'
  }
};

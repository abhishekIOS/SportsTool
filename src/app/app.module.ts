import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import 'hammerjs';
import { SharedModule } from './core/modules/shared.module';
import { AppComponent } from './app.component';
import { FuseFakeDbService } from './fuse-fake-db/fuse-fake-db.service';
import { FuseMainModule } from './main/main.module';
import { FuseSplashScreenService } from './core/services/splash-screen.service';
import { FuseConfigService } from './core/services/config.service';
import { FuseNavigationService } from './core/components/navigation/navigation.service';
import { TranslateModule } from '@ngx-translate/core';
import { AppStoreModule } from './store/store.module';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { StorageServiceModule } from 'angular-webstorage-service';
import { environment } from '../environments/environment';
import { STAppModule } from './st-components/st-app.module';
import { UploadService } from './st-services/upload/upload.service';
import { AuthService } from './st-services/auth/auth.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';
const appRoutes: Routes = [
    {
        path: 'apps',
        loadChildren: './main/content/apps/apps.module#FuseAppsModule'
    },
    {
        path: 'pages',
        loadChildren: './main/content/pages/pages.module#FusePagesModule'
    },
    {
        path: 'ui',
        loadChildren: './main/content/ui/ui.module#FuseUIModule'
    },
    {
        path: 'services',
        loadChildren: './main/content/services/services.module#FuseServicesModule'
    },
    {
        path: 'components',
        loadChildren: './main/content/components/components.module#FuseComponentsModule'
    },
    {
        path: 'components-third-party',
        loadChildren: './main/content/components-third-party/components-third-party.module#FuseComponentsThirdPartyModule'
    },
    {
        path: '**',
        redirectTo: '/home'
    }
];

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        HttpModule,
        BrowserAnimationsModule,
        RouterModule.forRoot(appRoutes),
        SharedModule,
        TranslateModule.forRoot(),
        InMemoryWebApiModule.forRoot(FuseFakeDbService, {
            delay: 0,
            passThruUnknownUrl: true
        }),
        AppStoreModule,
        FuseMainModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        AngularFireAuthModule,
        StorageServiceModule,
        STAppModule
    ],
    providers: [
        FuseSplashScreenService,
        FuseConfigService,
        FuseNavigationService,
        UploadService,
        AuthService
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}

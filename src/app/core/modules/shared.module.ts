import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ColorPickerModule } from 'ngx-color-picker';
import { NgxDnDModule } from '@swimlane/ngx-dnd';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { FuseMatSidenavHelperDirective, FuseMatSidenavTogglerDirective } from '../directives/fuse-mat-sidenav-helper/fuse-mat-sidenav-helper.directive';
import { FuseMatSidenavHelperService } from '../directives/fuse-mat-sidenav-helper/fuse-mat-sidenav-helper.service';
import { FusePipesModule } from '../pipes/pipes.module';
import { FuseConfirmDialogComponent } from '../components/confirm-dialog/confirm-dialog.component';
import { FuseCountdownComponent } from '../components/countdown/countdown.component';
import { FuseMatchMedia } from '../services/match-media.service';
import { FuseNavbarVerticalService } from '../../main/navbar/vertical/navbar-vertical.service';
import { FuseHighlightComponent } from '../components/highlight/highlight.component';
import { FusePerfectScrollbarDirective } from '../directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import { FuseIfOnDomDirective } from '../directives/fuse-if-on-dom/fuse-if-on-dom.directive';
import { FuseMaterialColorPickerComponent } from '../components/material-color-picker/material-color-picker.component';
import { FuseTranslationLoaderService } from '../services/translation-loader.service';
import { CookieService } from 'ngx-cookie-service';
import { TranslateModule } from '@ngx-translate/core';
import { MultiPhoneComponent } from '../../st-components/st-profile/multi-phone/multi-phone.component';
import { MultiEmailComponent } from '../../st-components/st-profile/multi-email/multi-email.component';
import { MultiUrlComponent } from '../../st-components/st-profile/multi-url/multi-url.component';
import { WebCamModule } from 'ack-angular-webcam';
import { WebCameraComponent } from '../../st-components/st-profile/web-camera/web-camera.component';
import { ReCaptchaModule } from 'angular2-recaptcha';
import { TimezonePickerModule } from 'ng2-timezone-selector';
import { TeamFormMenuComponent } from '../../st-components/st-team/team-form-menu/team-form-menu.component';
import { UserFormMenuComponent } from '../../st-components/st-users/user-form-menu/user-form-menu.component';
import { AgGridModule } from 'ag-grid-angular';
import { LogoComponent } from '../../st-components/ag-grid/logo/logo.component';
import { LoginConfirmComponent } from '../../st-components/login-confirm/login-confirm.component';
@NgModule({
    declarations   : [
        FuseMatSidenavHelperDirective,
        FuseMatSidenavTogglerDirective,
        FuseConfirmDialogComponent,
        FuseCountdownComponent,
        FuseHighlightComponent,
        FuseIfOnDomDirective,
        FusePerfectScrollbarDirective,
        FuseMaterialColorPickerComponent,
        MultiPhoneComponent,
        MultiEmailComponent,
        WebCameraComponent,
        MultiUrlComponent,
        TeamFormMenuComponent,
        UserFormMenuComponent,
        LogoComponent,
        LoginConfirmComponent
    ],
    imports        : [
        FlexLayoutModule,
        MaterialModule,
        CommonModule,
        FormsModule,
        FusePipesModule,
        ReactiveFormsModule,
        ColorPickerModule,
        NgxDnDModule,
        NgxDatatableModule,
        WebCamModule,
        ReCaptchaModule,
        TimezonePickerModule,
        AgGridModule.withComponents([
            TeamFormMenuComponent,
            UserFormMenuComponent,
            LogoComponent])
    ],
    exports        : [
        FlexLayoutModule,
        MaterialModule,
        CommonModule,
        FormsModule,
        FuseMatSidenavHelperDirective,
        FuseMatSidenavTogglerDirective,
        FusePipesModule,
        FuseCountdownComponent,
        FuseHighlightComponent,
        FusePerfectScrollbarDirective,
        ReactiveFormsModule,
        ColorPickerModule,
        NgxDnDModule,
        NgxDatatableModule,
        FuseIfOnDomDirective,
        FuseMaterialColorPickerComponent,
        TranslateModule,
        MultiPhoneComponent,
        MultiEmailComponent,
        TeamFormMenuComponent,
        UserFormMenuComponent,
        WebCameraComponent,
        WebCamModule,
        MultiUrlComponent,
        ReCaptchaModule,
        TimezonePickerModule,
        AgGridModule,
        LogoComponent
    ],
    entryComponents: [
        FuseConfirmDialogComponent,
        WebCameraComponent,
        LoginConfirmComponent
    ],
    providers      : [
        CookieService,
        FuseMatchMedia,
        FuseNavbarVerticalService,
        FuseMatSidenavHelperService,
        FuseTranslationLoaderService
    ]
})

export class SharedModule
{

}

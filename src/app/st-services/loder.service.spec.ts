import { TestBed, inject } from '@angular/core/testing';

import { LoderService } from './loder.service';

describe('LoderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoderService]
    });
  });

  it('should be created', inject([LoderService], (service: LoderService) => {
    expect(service).toBeTruthy();
  }));
});

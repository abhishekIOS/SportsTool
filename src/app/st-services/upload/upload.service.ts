import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import {Upload} from '../../st-models/upload.model';

@Injectable()
export class UploadService {

    db: any;
    constructor() {
        this.db = firebase.firestore();
    }


    pushFileToStorage(fileUpload: Upload, basePath: string, childPath: string) {
        const storageRef = firebase.storage().ref();
        const uploadTask = storageRef.child(`${basePath}/${fileUpload.key}/${fileUpload.file.name}`).put(fileUpload.file);
        uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
            (snapshot) => {
                // in progress
                const snap = snapshot as firebase.storage.UploadTaskSnapshot;
            },
            (error) => {
                // fail
                console.log(error);
                // if request.auth != null;
            },
            () => {
                // success
                fileUpload.url = uploadTask.snapshot.downloadURL;
                fileUpload.name = fileUpload.file.name;
                this.saveFileData(fileUpload, basePath, childPath);
                return fileUpload;
            }
        );
    }

    private saveFileData(fileUpload: Upload, basePath: string, childPath: string) {
        // this.db.collection(`${basePath}`).doc(fileUpload.key)
        //     .set({name: fileUpload.name, url: fileUpload.url, key: fileUpload.key});
        delete fileUpload.file;
        if (childPath === 'logo') {
            try {
                this.db.collection(basePath).doc(fileUpload.key).update({logo: Object.assign({}, fileUpload)});
            } catch (error) {
                console.log(error);
            }
        } else if (childPath === 'picture') {
            try {
                this.db.collection(basePath).doc(fileUpload.key).update({picture: Object.assign({}, fileUpload)});
            } catch (error) {
                console.log(error);
            }
        }

    }

    // getFileUploads(numberItems): AngularFireList<Upload> {
    //     return this.db.list(this.basePath, ref =>
    //         ref.limitToLast(numberItems));
    // }

    // deleteFileUpload(fileUpload: Upload) {
    //     this.deleteFileDatabase(fileUpload.key)
    //         .then(() => {
    //             this.deleteFileStorage(fileUpload.name);
    //         })
    //         .catch(error => console.log(error));
    // }

    private deleteFileDatabase(key: string, basePath: string) {
        return this.db.list(`${basePath}/`).remove(key);
    }

    deleteFileStorage(key: string, name: string, basePath: string) {
        const storageRef = firebase.storage().ref();
        storageRef.child(`${basePath}/${key}/${name}`).delete();
    }
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { LocationInfo} from '../../st-models/locationInfo';

@Injectable()
export class CountryService {

  constructor(private http: Http) { }

  getCountries(): Observable<any> {
      return this.http.get('../assets/country_list.json')
          .map(res => {
              return res.json();
          })
          .catch((e: any) => Observable.throw(this.errorHandler(e)));

  }

  getLocation(): Observable<any> {
    return this.http.get('https://ipapi.co/json/')
        .map(res => {
                let locationInfo = new LocationInfo();
                locationInfo.city = res.json().city;
                locationInfo.state = res.json().region;
                locationInfo.country = res.json().country_name;
                locationInfo.countryCode = res.json().country.toLowerCase();

                return locationInfo;


        })
        .catch((e: any) => Observable.throw(this.errorHandler(e)));
  }

    private errorHandler(error: any): void {
      console.log(error);
  }

}

import { Injectable, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import { Profile} from '../../st-models/profile.model';
@Injectable()
export class AuthService {

    authState: any = null;
    ref: any;
    constructor(private afAuth: AngularFireAuth,
                private router: Router,
                @Inject(SESSION_STORAGE) private storage: StorageService) {
        this.afAuth.authState.subscribe((auth) => {
            this.authState = auth;
        });
        this.ref = firebase.firestore().collection('profiles');
    }

    get isUserAnonymousLoggedIn(): boolean {
        return (this.authState !== null) ? this.authState.isAnonymous : false;
    }

    currentUserId(): string {
        return (this.authState !== null) ? this.authState.uid : null;
    }

    get currentUserName(): string {
        return this.authState['email'];
    }

    currentUser(): any {
        return (this.authState !== null) ? this.authState : null;
    }

    get isUserEmailLoggedIn(): boolean {
        if ((this.authState !== null) && (!this.isUserAnonymousLoggedIn)) {
            return true;
        } else {
            return false;
        }
    }

    async setUserprofile(newuser, name): Promise<any>{
        try {
            // set firebase user displayname
            await newuser.updateProfile({
                displayName: name,
                // photoURL: "https://example.com/jane-q-user/profile.jpg"
            });
            try {
                // register new profile
                const newprofile = new Profile();
                const newmail = {role: null, email: newuser.email};
                newprofile.uid = newuser.uid;
                newprofile.nickName = name;
                newprofile.emails.push(newmail);
                newprofile.date_create = new Date();
                newprofile.adaptive = Object.assign({}, newprofile.adaptive);
                await this.ref.doc(this.authState.uid).set(Object.assign({}, newprofile));
                return true;
            } catch (error) {
                await console.log(error);
                return error;
            }
        } catch (error) {
            await console.log(error);
            return error;
        }
    }

    async signUpWithEmail(email: string, password: string, name: string): Promise<any> {
        try {
            const newuser = await this.afAuth.auth.createUserWithEmailAndPassword(email, password);
            this.authState = newuser;
            await this.setUserprofile(newuser, name);
            return true;
        } catch (error) {
            await console.log(error);
            return error;
        }
    }

    async loginWithEmail(email: string, password: string): Promise<any>{
        try {
            const loginUser = await this.afAuth.auth.signInWithEmailAndPassword(email, password);
            this.authState = loginUser;
            this.storage.set('uid', this.authState.uid);
            return loginUser;
        } catch (error) {
            await console.log(error);
            return error;
        }
    }

    async signInWithGoogle(): Promise<any>{
        try {
            const result = await this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
            this.authState = result.user;
            // check user
            const docRef = await this.ref.doc(this.authState.uid).get();
                if (docRef.exists) {
                    // set signed user session
                    this.storage.set('uid', this.authState.uid);
                } else {
                    // register new profile
                    const newprofile = new Profile();
                    const newmail = {role: null, email: result.user.email};
                    newprofile.uid = result.user.uid;
                    newprofile.nickName = result.user.displayName;
                    newprofile.emails.push(newmail);
                    newprofile.date_create = new Date();
                    newprofile.adaptive = Object.assign({}, newprofile.adaptive);
                    await this.ref.doc(this.authState.uid).set(Object.assign({}, newprofile));
                    // set signed user session
                    this.storage.set('uid', this.authState.uid);
                }
            return result.user;
        } catch (error) {
            await console.log(error);
            return error;
        }

    }

    async signInWithFacebook(): Promise<any>{
        try {
            const result = await this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider());
            this.authState = result.user;
            // check user
            const docRef = await this.ref.doc(this.authState.uid).get();
            if (docRef.exists) {
                // set signed user session
                this.storage.set('uid', this.authState.uid);
            } else {
                // register new profile
                const newprofile = new Profile();
                const newmail = {role: null, email: result.user.email};
                newprofile.uid = result.user.uid;
                newprofile.nickName = result.user.displayName;
                newprofile.emails.push(newmail);
                newprofile.date_create = new Date();
                newprofile.adaptive = Object.assign({}, newprofile.adaptive);
                await this.ref.doc(this.authState.uid).set(Object.assign({}, newprofile));
                // set signed user session
                this.storage.set('uid', this.authState.uid);
            }
            return result.user;
        } catch (error) {
            await console.log(error);
            return error;
        }

    }

    signOut(): void {
        this.afAuth.auth.signOut();
        this.storage.remove('uid');
        this.router.navigate(['/home']);
    }

}

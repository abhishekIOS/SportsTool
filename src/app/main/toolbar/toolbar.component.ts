import { Component, Inject } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { FuseConfigService } from '../../core/services/config.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../../st-services/auth/auth.service';
import { UserService } from '../../st-components/st-profile/shared/user.service';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import * as firebase from 'firebase';
import { CountryService } from '../../st-services/country/country.service';

@Component({
    selector   : 'fuse-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls  : ['./toolbar.component.scss'],
    providers: [UserService, CountryService]
})

export class FuseToolbarComponent
{
    userStatusOptions: any[];
    languages: any;
    selectedLanguage: any;
    showLoadingBar: boolean;
    horizontalNav: boolean;
    currentuser: any;
    countries: any = [];
    constructor(
        private router: Router,
        private fuseConfig: FuseConfigService,
        private translate: TranslateService,
        public countryservice: CountryService,
        public authService: AuthService,
        @Inject(SESSION_STORAGE) private storage: StorageService
    )
    {
        this.countryservice.getCountries().subscribe(
            resultArray => {this.countries = resultArray;
                // this.countries = this.countries.find(c => c.Code.toLowerCase() === 'en');
                },
            error => console.log('Error :: ' + error)
        );

        this.userStatusOptions = [
            {
                'title': 'Online',
                'icon' : 'icon-checkbox-marked-circle',
                'color': '#4CAF50'
            },
            {
                'title': 'Away',
                'icon' : 'icon-clock',
                'color': '#FFC107'
            },
            {
                'title': 'Do not Disturb',
                'icon' : 'icon-minus-circle',
                'color': '#F44336'
            },
            {
                'title': 'Invisible',
                'icon' : 'icon-checkbox-blank-circle-outline',
                'color': '#BDBDBD'
            },
            {
                'title': 'Offline',
                'icon' : 'icon-checkbox-blank-circle-outline',
                'color': '#616161'
            }
        ];

        this.languages = [
            {
                'id'   : 'en',
                'title': 'English',
                'flag' : 'us'
            },
            // {
            //     'id'   : 'tr',
            //     'title': 'Turkish',
            //     'flag' : 'tr'
            // },
            {
                'id'   : 'cn',
                'title': 'China',
                'flag' : 'cn'
            },
            {
                'id'   : 'pt',
                'title': 'Portugesee',
                'flag' : 'pt'
            },
            {
                'id'   : 'es',
                'title': 'Spanish',
                'flag' : 'es'
            },
            {
                'id'   : 'pf',
                'title': 'French',
                'flag' : 'pf'
            },
            {
                'id'   : 'sz',
                'title': 'Swahili',
                'flag' : 'sz'
            },
            {
                'id'   : 'in',
                'title': 'Hindi',
                'flag' : 'in'
            }
        ];

        this.selectedLanguage = this.languages[0];

        router.events.subscribe(
            (event) => {
                if ( event instanceof NavigationStart )
                {
                    this.showLoadingBar = true;
                }
                if ( event instanceof NavigationEnd )
                {
                    this.showLoadingBar = false;
                }
            });

        this.fuseConfig.onSettingsChanged.subscribe((settings) => {
            this.horizontalNav = settings.layout.navigation === 'top';
        });

        // get current user with signed user
        const userid = this.storage.get('uid');
        if (userid) {
            // -------not realtime data binding, only get data---------//
            // firebase.firestore().collection('profiles').doc(userid).get()
            //     .then((doc) => {
            //         this.currentuser = doc.data();
            //     });

            // -------realtime data binding---------//
            firebase.firestore().collection('profiles').doc(userid).onSnapshot
                ({includeMetadataChanges: true}, (doc) => {
                    this.currentuser = doc.data();
                });
        }


    }

    signOut(): void {
        // SIGN OUT
        this.currentuser = null;
        this.authService.signOut();
    }

    search(value)
    {
        // Do your search here...
        console.log(value);
    }

    setLanguage(lang)
    {
        // Set the selected language for toolbar
        this.selectedLanguage = lang;

        // Use the selected language for translations
        this.translate.use(lang.id);
    }
}

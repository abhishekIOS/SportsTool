import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Club} from '../../../st-models/club.model';
import { UserFormComponent } from '../user-form/user-form.component';
import { AngularFirestore } from 'angularfire2/firestore';
import { fuseAnimations } from '../../../core/animations';
import * as firebase from 'firebase';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import {UploadService} from '../../../st-services/upload/upload.service';
import {Upload} from '../../../st-models/upload.model';
import { CountryService } from '../../../st-services/country/country.service';
import 'moment';
declare let moment: any;
@Component({
  selector: 'fuse-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
    providers: [CountryService],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
})
export class UsersComponent implements OnInit {

    item: Observable<any>;
    users: any = [];
    imagepath: any;
    hasSelectedContacts: boolean;
    searchInput: FormControl;
    dialogRef: any;
    currentUserID: string;
    FileUpload: Upload;
    countries: any = [];

    constructor( private afs: AngularFirestore,
                 public dialog: MatDialog,
                 @Inject(SESSION_STORAGE) private storage: StorageService,
                 private uploadService: UploadService,
                 private countryservice: CountryService)
    {
        this.searchInput = new FormControl('');
        this.currentUserID = this.storage.get('uid');
        this.countryservice.getCountries().subscribe(
            resultArray => this.countries = resultArray,
            error => console.log('Error :: ' + error)
        );
    }

    ngOnInit() {

    }

    newUser()
    {
        this.dialogRef = this.dialog.open(UserFormComponent, {
            panelClass: 'contact-form-dialog',
            data      : {
                countries: this.countries,
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if ( !response || !this.currentUserID )
                {
                    return;
                }
                const formData: FormGroup = response[0];
                const logoimage: FormGroup = response[1];
                this.addNewUser(formData.value, logoimage);
            });

    }

    async addNewUser(user, logo): Promise<void> {
        // set unique ID
        const clubid =
            moment().format('YYMMMDDTHHmmss') +
            '-' +
            Math.random()
                .toString(36)
                .slice(2);
        user.id = clubid;
        await firebase.firestore().collection('profiles').doc(clubid).set(Object.assign({}, user));
        if (logo) {
            this.FileUpload = new Upload(logo);
            this.FileUpload.key = user.id;
            user.logo = await this.uploadService.pushFileToStorage(this.FileUpload, 'profiles', 'logo');

        }
    }
}

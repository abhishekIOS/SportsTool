import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'fuse-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class UserDetailComponent implements OnInit {

    user: any;
    constructor( public dialogRef: MatDialogRef<UserDetailComponent>,
                 @Inject(MAT_DIALOG_DATA) private data: any) {
        this.user = data.user;
    }

    ngOnInit() {
    }

}

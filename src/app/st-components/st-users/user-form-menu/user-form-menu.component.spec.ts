import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserFormMenuComponent } from './user-form-menu.component';

describe('UserFormMenuComponent', () => {
  let component: UserFormMenuComponent;
  let fixture: ComponentFixture<UserFormMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserFormMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFormMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnDestroy, Inject} from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular';
import { MatDialog, MatDialogRef } from '@angular/material';
import { UserFormComponent } from '../user-form/user-form.component';
import { UserDetailComponent } from '../user-detail/user-detail.component';
import { FuseConfirmDialogComponent } from '../../../core/components/confirm-dialog/confirm-dialog.component';
import { LoginConfirmComponent } from '../../login-confirm/login-confirm.component';
import { UploadService } from '../../../st-services/upload/upload.service';
import * as firebase from 'firebase';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import { Router } from '@angular/router';
import { Upload } from '../../../st-models/upload.model';

@Component({
  selector: 'fuse-user-form-menu',
  templateUrl: './user-form-menu.component.html',
  styleUrls: ['./user-form-menu.component.scss']
})
export class UserFormMenuComponent implements ICellRendererAngularComp, OnDestroy {

    data: any;
    dialogRef: any;
    disabled = true;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    loginconfirmDialogRef: MatDialogRef<LoginConfirmComponent>;
    constructor(public dialog: MatDialog,
                private uploadService: UploadService,
                @Inject(SESSION_STORAGE) private storage: StorageService,
                private router: Router) {

    }
    agInit(params: any): void {
        this.data = params.data;
        if (this.storage.get('uid') === this.data.owner_id) {
            this.disabled = false;
        } else {
            this.disabled = true;
        }
    }

    ngOnDestroy() {
        console.log(`Destroying SquareComponent`);
    }

    detailItem()
    {
        this.dialogRef = this.dialog.open(UserDetailComponent, {
            panelClass: 'contact-form-dialog',
            data      : {
                user: this.data,
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {

            });
    }

    editItem()
    {
        this.dialogRef = this.dialog.open(UserFormComponent, {
            panelClass: 'contact-form-dialog',
            data      : {
                user: Object.assign({}, this.data),
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const formData: any = response[0];
                const editdata: any = response[1];
                const logoimage: any = response[2];
                const picturefile: any = response[3];
                this.updateTeam(formData.value, editdata, logoimage, picturefile);

            });
    }

    async updateTeam(formdata, team, logo, picturefile): Promise<void> {

        team.date_update = new Date();
        team.adaptive.items = formdata.adaptive_items;
        await firebase.firestore().collection('teams').doc(team.id).update(Object.assign({}, team));
        if (logo) {
            const logofile = new Upload(logo);
            logofile.key = team.id;
            team.logo = await this.uploadService.pushFileToStorage(logofile, 'teams', 'logo');

        }

        if (picturefile) {
            const picture = new Upload(picturefile);
            picture.key = team.id;
            team.description = await this.uploadService.pushFileToStorage(picture, 'teams', 'picture');

        }
    }

    deleteItem()
    {
        if (this.storage.get('uid')) {
            this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
                disableClose: false
            });

            this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

            this.confirmDialogRef.afterClosed().subscribe(result => {
                if ( result )
                {
                    firebase.firestore().collection('teams').doc(this.data.id).delete();
                    if (this.data.logo) {
                        this.uploadService.deleteFileStorage(this.data.logo.key, this.data.logo.name, 'teams');
                    }
                    if (this.data.description) {
                        this.uploadService.deleteFileStorage(this.data.description.key, this.data.description.name, 'teams');
                    }
                }
                this.confirmDialogRef = null;
            });
        } else {
            this.loginconfirmDialogRef = this.dialog.open(LoginConfirmComponent, {
                disableClose: false
            });

            this.loginconfirmDialogRef.componentInstance.confirmMessage = 'Sorry, this action is needed login on site';

            this.loginconfirmDialogRef.afterClosed().subscribe(result => {
                if ( result ) {
                    this.router.navigate(['/login']);
                }
                this.loginconfirmDialogRef = null;
            });
        }
    }

    refresh(): boolean {
        return false;
    }

}

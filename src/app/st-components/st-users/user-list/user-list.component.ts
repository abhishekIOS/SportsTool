import { Component, OnInit, ViewEncapsulation, Input, ViewChild } from '@angular/core';
import { fuseAnimations } from '../../../core/animations';
import * as firebase from 'firebase';
import { UserFormMenuComponent } from '../user-form-menu/user-form-menu.component';
import { LogoComponent } from '../../ag-grid/logo/logo.component';
@Component({
  selector: 'fuse-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations,
})
export class UserListComponent implements OnInit {

    @Input() currentUserID: string;
    users: any[] = [];
    filter: any = {
        name: '',
        shirt_color: '',
        gender: ''
    };
    columnDefs;
    context;
    rowData: any[];
    frameworkComponents = {
        UserFormMenuComponent: UserFormMenuComponent,
        LogoComponent: LogoComponent
    };
    constructor() {
        this.columnDefs = [
            {headerName: 'Logo',
                field: 'logo',
                cellRenderer: 'LogoComponent',
                suppressSorting: false,
                suppressMenu: true,
                width: 80
            },
            {headerName: 'name', field: 'nickName'},
            {headerName: 'gender', field: 'gender' },
            {headerName: 'age', field: 'age' },
            {headerName: 'sports', field: 'sports' },
            {
                headerName: '',
                // field: 'make',
                editable: false,
                cellRenderer: 'UserFormMenuComponent',
                // colId: 'make',
                width: 70
            },

        ];
        this.context = { componentParent: this };

    }

    async ngOnInit() {
        await this.getAllUsers();
    }

    async getAllUsers() {
        const query = firebase.firestore().collection('profiles');
        query.onSnapshot((snapshot) => {
            this.users = [];
            snapshot.forEach((doc) => {
                this.users.push(doc.data());
            });
            this.rowData = this.users;
        }, (error) => {
            console.log(error);
        });
    }

}

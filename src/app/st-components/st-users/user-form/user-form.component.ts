import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormControl, FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Profile } from '../../../st-models/profile.model';
import { Observable } from 'rxjs/Observable';
import { AngularFirestore } from 'angularfire2/firestore';
import { fuseAnimations } from '../../../core/animations';
import { WebCameraComponent } from '../../st-profile/web-camera/web-camera.component';
import * as firebase from 'firebase';
import 'moment';
declare let moment: any;

@Component({
  selector: 'fuse-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
})
export class UserFormComponent implements OnInit {

    event: CalendarEvent;
    dialogTitle: string;
    form: FormGroup;
    formErrors: any;
    action: string;
    user: Profile;
    logoURL: any = null;
    docURL: any = null;
    cameradialogRef: any;

    selectedFiles: any;
    selectedDocFiles: any;
    countries: any = [];
    roles$: Observable<any>;
    filteredCountries: any = [];
    countryCtrl: FormControl;
    sports$: Observable<any>;
    constructor( public dialogRef: MatDialogRef<UserFormComponent>,
                 @Inject(MAT_DIALOG_DATA) private data: any,
                 public dialog: MatDialog,
                 private formBuilder: FormBuilder,
                 private afs: AngularFirestore) {
        this.selectedFiles = null;
        this.selectedDocFiles = null;
        this.logoURL = null;
        this.docURL = null;
        this.action = data.action;
        this.countryCtrl = new FormControl();
        this.sports$ = this.afs.collection<any>('sports').valueChanges();
        this.roles$ = this.afs.collection<any>('roles').valueChanges();

        this.formErrors = {
            firstName: {},
            lastName: {},
            nickName: {},
            gender: {},
            birthday: {},
            cityOfBirth: {},
            countryOfBirth: {},
            sports: {},
            roles: {},
            emails: {},
            phones: {},
            address: {},
            address2: {},
            city: {},
            state: {},
            postalCode: {},
            country: {},
            adaptive_status: {},
            adaptive_items: {},
            adaptive_note: {},
        };

        this.form = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            nickName: [''],
            gender: ['', Validators.required],
            birthday: ['', Validators.required],
            cityOfBirth: [''],
            countryOfBirth: [''],
            sports: ['', Validators.required],
            roles: ['', Validators.required],
            phones: this.formBuilder.array([ this.createItem('phone')]),
            emails: this.formBuilder.array([ this.createItem('email')]),
            address: ['', Validators.required],
            address2: [''],
            city: ['', Validators.required],
            state: ['', Validators.required],
            postalCode: ['', [Validators.required, Validators.maxLength(5)]],
            country: ['', Validators.required],
            adaptive_status: [''],
            adaptive_items: this.formBuilder.array([ this.createItem('adapt')]),
            adaptive_note: ['']
        });

        if ( this.action === 'edit' )
        {
            this.dialogTitle = 'Edit User';
            this.user = data.user;
            this.countries = data.countries;
            this.filteredCountries = this.countries;
            if (this.user.emails.length > 0) {
                for (let i = 0; i < this.user.emails.length; i++) {
                    this.emails.push(this.formBuilder.group({
                        role: [this.user.emails[i].role],
                        email: [this.user.emails[i].email]
                    }));
                }
                this.emails.removeAt(0);
            }

            if (this.user.phones.length > 0) {
                for (let i = 0; i < this.user.phones.length; i++) {
                    this.phones.push(this.formBuilder.group({
                        role: [this.user.phones[i].role],
                        phone: [this.user.phones[i].phone]
                    }));
                }
                this.phones.removeAt(0);

            }

            if (this.user.adaptive.items.length > 0) {
                for (let i = 0; i < this.user.adaptive.items.length; i++) {
                    this.adaptive_items.push(this.formBuilder.group({
                        name: [this.user.adaptive.items[i].name],
                        status: [this.user.adaptive.items[i].status],
                        value: [this.user.adaptive.items[i].value]
                    }));
                }
                this.adaptive_items.removeAt(0);
            }

            if (this.user.logo) {
                this.logoURL = this.user.logo.url;
            }
            if (this.user.picture) {
                this.docURL = this.user.picture.url;
            }
        }
        else
        {
            this.dialogTitle = 'New User';
            this.user = new Profile();
            this.countries = data.countries;
            this.filteredCountries = this.countries;
        }

    }

    ngOnInit() {

        this.form.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
            if (this.form.value.birthday) {
                const bday = new Date(moment(this.form.value.birthday));
                const age = this.calculateAge(bday.getMonth(), bday.getDate(), bday.getFullYear());
                this.user.age = age;
            }
            if (this.form.value.country) {
                this.filteredCountries = this.filterCountries(this.form.value.country);
            }
        });
    }

    calculateAge(birthMonth, birthDay, birthYear): number{
        const currentDate = new Date();
        const currentYear = currentDate.getFullYear();
        const currentMonth = currentDate.getMonth();
        const currentDay = currentDate.getDate();
        let calculatedAge = currentYear - birthYear;
        if (currentMonth < birthMonth ) {
            calculatedAge--;
        }
        if (birthMonth  === currentMonth && currentDay < birthDay) {
            calculatedAge--;
        }
        return calculatedAge;
    }

    onFormValuesChanged()
    {
        for ( const field in this.formErrors )
        {
            if ( !this.formErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.formErrors[field] = control.errors;
            }
        }
    }

    async selectlogo(event): Promise<void> {
        this.selectedFiles = event.target.files.item(0);
        this.logoURL = await this.getBase64(event.target.files.item(0));
    }

    async selectDoc(event): Promise<void> {
        this.selectedDocFiles = event.target.files.item(0);
        this.docURL = await this.getBase64(event.target.files.item(0));
    }

    async getBase64(file): Promise<any> {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }

    applyFilter(value) {
        if (value) {
            this.filteredCountries = this.filterCountries(value);
        }
    }

    filterCountries(name: string) {
        return this.countries.filter(country =>
            country.Name.toLowerCase().indexOf(name.toLowerCase()) === 0);
    }

    createItem(item): FormGroup {
        if (item === 'phone') {
            return this.formBuilder.group({
                role: [''],
                phone: ['', Validators.required]
            });
        }
        else if (item === 'email') {
            return this.formBuilder.group({
                role: [''],
                email: ['', Validators.required]
            });
        }
        else if (item === 'adapt') {
            return this.formBuilder.group({
                name: [''],
                status: [''],
                value: ['']
            });
        }

    }

    get phones(): FormArray {
        return this.form.get('phones') as FormArray;
    }

    get emails(): FormArray {
        return this.form.get('emails') as FormArray;
    }

    get adaptive_items(): FormArray {
        return this.form.get('adaptive_items') as FormArray;
    }

    deleteItem(item, index): void {
        if (index === 0) {
            return;
        }
        if (item === 'phone') {
            this.phones.removeAt(index);
        }
        else if (item === 'email') {
            this.emails.removeAt(index);
        }
    }

    addItem(item): void {
        if (item === 'phone') {
            this.phones.push(this.createItem(item));
        }
        else if (item === 'email') {
            this.emails.push(this.createItem(item));
        }
    }

    cameracapture()
    {
        this.cameradialogRef = this.dialog.open(WebCameraComponent, {
            panelClass: 'fuse-web-camera',
            data      : {
                profile: this.user,
            },
            height: '600px',
            width: '300px',
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }

                this.logoURL = response[0];
                this.selectedFiles = response[1];
            });
    }

    doccameracapture()
    {
        this.cameradialogRef = this.dialog.open(WebCameraComponent, {
            panelClass: 'fuse-web-camera',
            data      : {
                profile: this.user,
            },
            height: '600px',
            width: '300px',
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }

                this.docURL = response[0];
                this.selectedDocFiles = response[1];
            });
    }

}

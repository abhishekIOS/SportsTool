import { NgModule } from '@angular/core';
import { SharedModule } from '../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';

import { UsersComponent } from './users/users.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { NewUserComponent } from './new-user/new-user.component';
import { MultiEmailComponent } from '../st-profile/multi-email/multi-email.component';
import { MultiPhoneComponent } from '../st-profile/multi-phone/multi-phone.component';
import { WebCameraComponent } from '../st-profile/web-camera/web-camera.component';
const routes: Routes = [
    {
        path     : '',
        component: UsersComponent,

    },
    {
        path     : 'newuser',
        component: NewUserComponent
    },
    {
        path     : 'allusers',
        component: UsersComponent
    }

];

@NgModule({
    declarations: [
        UsersComponent,
        UserListComponent,
        UserFormComponent,
        UserDetailComponent,
        NewUserComponent
    ],
    imports     : [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    entryComponents: [UserFormComponent, UserDetailComponent]
})

export class StUsersModule
{

}

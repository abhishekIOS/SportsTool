import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { UserService } from '../../st-profile/shared/user.service';
import { Profile} from '../../../st-models/profile.model';
import { AngularFirestore } from 'angularfire2/firestore';
import {LoaderService} from '../../../st-services/loder.service';
import { CountryService } from '../../../st-services/country/country.service';
import {Upload} from '../../../st-models/upload.model';
import { fuseAnimations } from '../../../core/animations';
import { Observable } from 'rxjs/Observable';
import 'moment';
declare let moment: any;
import * as _ from 'lodash';
import { MatDialog, MatDialogRef } from '@angular/material';
import { WebCameraComponent } from '../../st-profile/web-camera/web-camera.component';
import { Router } from '@angular/router';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import { MatSnackBar } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../core/components/confirm-dialog/confirm-dialog.component';
import * as firebase from 'firebase';

@Component({
  selector: 'fuse-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss'],
    providers: [UserService, CountryService, LoaderService],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
})
export class NewUserComponent implements OnInit {

    newuserform: FormGroup;
    formErrors: any;
    item: Observable<any>;
    sports$: Observable<any>;
    roles$: Observable<any>;
    countries$: any = [];
    filteredCountries: any = [];
    birthfilteredCountries: any = [];
    newuser: Profile;
    profiles: any = [];
    logoURL: any = null;
    docURL: any = null;
    dialogRef: any;

    selectedFiles: any;
    selectedDocFiles: any;
    disableBtn = false;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    constructor( private formBuilder: FormBuilder, public dialog: MatDialog,
                 private afs: AngularFirestore, private userService: UserService,
                 @Inject(SESSION_STORAGE) private storage: StorageService,
                 public snackBar: MatSnackBar,
                 private countryservice: CountryService) {

        // Reactive form errors
        this.formErrors = {
            firstName: {},
            lastName: {},
            nickName: {},
            gender: {},
            birthday: {},
            cityOfBirth: {},
            countryOfBirth: {},
            sports: {},
            roles: {},
            emails: {},
            phones: {},
            address: {},
            address2: {},
            city: {},
            state: {},
            postalCode: {},
            country: {},
            adaptive_status: {},
            adaptive_items: {},
            adaptive_note: {},
            doc_category: {}
        };
        this.selectedFiles = null;
        this.selectedDocFiles = null;
        this.countryservice.getCountries().subscribe(
            resultArray => this.countries$ = resultArray,
            error => console.log('Error :: ' + error)
        );
        firebase.firestore().collection('AdaptItems').onSnapshot((snapshot) => {
            snapshot.forEach((doc) => {
                this.adaptive_items.push(this.formBuilder.group({
                    name: [doc.data().name],
                    status: [false],
                    value: [1]
                }));
            });
            this.adaptive_items.removeAt(0);
        }, (error) => {
            console.log(error);
        });
        this.newuser = new Profile();
        // this will subscribe and console.log all the users in firestore upon
        this.sports$ = this.afs.collection<any>('sports').valueChanges();
        this.roles$ = this.afs.collection<any>('roles').valueChanges();
    }

    async ngOnInit(): Promise<void> {
        // Reactive Form
        this.newuserform = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            nickName: [''],
            gender: ['', Validators.required],
            birthday: ['', Validators.required],
            cityOfBirth: [''],
            countryOfBirth: [''],
            sports: ['', Validators.required],
            roles: ['', Validators.required],
            emails: this.formBuilder.array([ this.createItem('email')]),
            phones: this.formBuilder.array([ this.createItem('phone')]),
            address: ['', Validators.required],
            address2: [''],
            city: ['', Validators.required],
            state: ['', Validators.required],
            postalCode: ['', [Validators.required, Validators.maxLength(5)]],
            country: ['', Validators.required],
            adaptive_status: [''],
            adaptive_items: this.formBuilder.array([ this.createItem('adapt')]),
            adaptive_note: [''],
            description: [''],
            doc_category: ['']
        });

        await this.countryservice.getLocation().subscribe(
            result => this.setcountry(result.country),
            error => console.log('Error :: ' + error)
        );
        this.newuser.roles.push('Player');

        this.newuserform.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
            if (this.newuserform.value.birthday) {
                const bday = new Date(moment(this.newuserform.value.birthday));
                const age = this.calculateAge(bday.getMonth(), bday.getDate(), bday.getFullYear());
                this.newuser.age = age;
            }
            if (this.newuserform.value.country) {
                this.filteredCountries = this.filterCountries(this.newuserform.value.country);
            }
            if (this.newuserform.value.countryOfBirth) {
                this.birthfilteredCountries = this.filterCountries(this.newuserform.value.countryOfBirth);
            }
        });
    }

    setcountry(country) {
        if (country) {
            this.newuser.country = country;
            this.newuser.countryOfBirth = country;
        } else {
            this.newuser.country = 'United States';
            this.newuser.countryOfBirth = 'United States';
        }
    }

    createItem(item): FormGroup {
        if (item === 'phone') {
            return this.formBuilder.group({
                role: [''],
                phone: ['', Validators.required]
            });
        }
        else if (item === 'email') {
            return this.formBuilder.group({
                role: [''],
                email: ['', Validators.required]
            });
        }
        else if (item === 'adapt') {
            return this.formBuilder.group({
                name: [''],
                status: [''],
                value: ['']
            });
        }

    }

    get phones(): FormArray {
        return this.newuserform.get('phones') as FormArray;
    }

    get emails(): FormArray {
        return this.newuserform.get('emails') as FormArray;
    }
    get adaptive_items(): FormArray {
        return this.newuserform.get('adaptive_items') as FormArray;
    }
    deleteItem(item, index): void {
        if (index === 0) {
            return;
        }
        if (item === 'phone') {
            this.phones.removeAt(index);
        }
        else if (item === 'email') {
            this.emails.removeAt(index);
        }
    }

    addItem(item): void {
        if (item === 'phone') {
            this.phones.push(this.createItem(item));
        }
        else if (item === 'email') {
            this.emails.push(this.createItem(item));
        }
    }

    calculateAge(birthMonth, birthDay, birthYear): number{
        const currentDate = new Date();
        const currentYear = currentDate.getFullYear();
        const currentMonth = currentDate.getMonth();
        const currentDay = currentDate.getDate();
        let calculatedAge = currentYear - birthYear;
        if (currentMonth < birthMonth ) {
            calculatedAge--;
        }
        if (birthMonth  === currentMonth && currentDay < birthDay) {
            calculatedAge--;
        }
        return calculatedAge;
    }

    filterCountries(name: string) {
        return this.countries$.filter(country =>
            country.Name.toLowerCase().indexOf(name.toLowerCase()) === 0);
    }
    async onSubmit(): Promise<void> {
        if (this.newuserform.invalid) {
            return;
        }
        this.disableBtn = true;
        this.newuser.emails = this.newuserform.value.emails;
        this.newuser.phones = this.newuserform.value.phones;
        this.newuser.adaptive.items = this.newuserform.value.adaptive_items;
        this.newuser.adaptive = Object.assign({}, this.newuser.adaptive);
        const result = await this.userService.addnewuser(this.newuser, this.selectedFiles, this.selectedDocFiles);
        if (result) {
            this.disableBtn = false;
            this.snackBar.open('update success!', 'Dance', {
                duration: 2000
            });
        } else {
            this.disableBtn = false;
            this.snackBar.open('update failed!', 'Dance', {
                duration: 2000
            });
        }

    }
    async selectlogo(event): Promise<void> {
        this.selectedFiles = event.target.files.item(0);
        this.logoURL = await this.getBase64(event.target.files.item(0));
    }

    async selectDoc(event): Promise<void> {
        this.selectedDocFiles = event.target.files.item(0);
        this.docURL = await this.getBase64(event.target.files.item(0));
    }

    async getBase64(file): Promise<any> {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }

    reset() {
        this.newuser = new Profile();
    }

    onFormValuesChanged()
    {
        for ( const field in this.formErrors )
        {
            if ( !this.formErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.newuserform.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.formErrors[field] = control.errors;
            }
        }
    }

    cameracapture()
    {
        this.dialogRef = this.dialog.open(WebCameraComponent, {
            panelClass: 'fuse-web-camera',
            data      : {
                profile: this.newuser,
            },
            height: '600px',
            width: '300px',
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }

                this.logoURL = response[0];
                this.selectedFiles = response[1];
            });
    }

    doccameracapture()
    {
        this.dialogRef = this.dialog.open(WebCameraComponent, {
            panelClass: 'fuse-web-camera',
            data      : {
                profile: this.newuser,
            },
            height: '600px',
            width: '300px',
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }

                this.docURL = response[0];
                this.selectedDocFiles = response[1];
            });
    }
}


import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
    selector   : 'fuse-login-confirm',
    templateUrl: './login-confirm.component.html',
    styleUrls: ['./login-confirm.component.scss']
})
export class LoginConfirmComponent implements OnInit
{
    public confirmMessage: string;

    constructor(public dialogRef: MatDialogRef<LoginConfirmComponent>)
    {
    }

    ngOnInit()
    {
    }

}

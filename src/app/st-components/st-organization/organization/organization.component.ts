import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { fuseAnimations } from '../../../core/animations';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';

@Component({
  selector: 'fuse-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
})
export class OrganizationComponent implements OnInit {

    currentUserID: string;

    constructor(@Inject(SESSION_STORAGE) private storage: StorageService)
    {
        this.currentUserID = this.storage.get('uid');
    }

    ngOnInit() {

    }
}

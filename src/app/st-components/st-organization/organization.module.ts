import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../core/modules/shared.module';
import { NewOrganizationComponent } from './new-organization/new-organization.component';
import { OrganizationComponent } from './organization/organization.component';
import { OrganizationDetailComponent } from './organization-detail/organization-detail.component';
import { OrganizationFormComponent } from './organization-form/organization-form.component';
import { OrganizationListComponent } from './organization-list/organization-list.component';
const routes = [
  {
    path: '',
    component: OrganizationComponent
  },
    {
        path     : 'neworganization',
        component: NewOrganizationComponent
    },
    {
        path     : 'allorganizations',
        component: OrganizationComponent
    }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)],
  declarations: [OrganizationComponent,
      OrganizationFormComponent,
      OrganizationListComponent,
      OrganizationDetailComponent,
      NewOrganizationComponent],
  entryComponents: [OrganizationFormComponent, OrganizationDetailComponent]

})
export class OrganizationModule {}

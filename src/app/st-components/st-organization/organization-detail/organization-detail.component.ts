import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import * as firebase from 'firebase';

@Component({
  selector: 'fuse-organization-detail',
  templateUrl: './organization-detail.component.html',
  styleUrls: ['./organization-detail.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class OrganizationDetailComponent implements OnInit {

    organization: any;
    owner: any;
    constructor( public dialogRef: MatDialogRef<OrganizationDetailComponent>,
                 @Inject(MAT_DIALOG_DATA) private data: any) {
        this.organization = data.organization;
        if (this.organization.owner_id) {
            firebase.firestore().collection('profiles').doc(this.organization.owner_id).get().then((doc) => {
                this.owner = doc.data();
            });

        }
    }

    ngOnInit() {
    }

}

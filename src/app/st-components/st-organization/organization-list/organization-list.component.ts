import { Component, OnInit, Input, ViewEncapsulation, ViewChild } from '@angular/core';
import { OrganizationFormComponent } from '../organization-form/organization-form.component';
import { MatDialog, MatDialogRef } from '@angular/material';
import { fuseAnimations } from '../../../core/animations';
import {MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import * as firebase from 'firebase';
import {UploadService} from '../../../st-services/upload/upload.service';
import { CountryService } from '../../../st-services/country/country.service';
import {Upload} from '../../../st-models/upload.model';
import { FuseConfirmDialogComponent } from '../../../core/components/confirm-dialog/confirm-dialog.component';
import { OrganizationDetailComponent } from '../organization-detail/organization-detail.component';

@Component({
  selector: 'fuse-organization-list',
  templateUrl: './organization-list.component.html',
  styleUrls: ['./organization-list.component.scss'],
    providers: [CountryService],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
})
export class OrganizationListComponent implements OnInit {

    @Input() currentUserID: string;
    organizations: any[] = [];
    filteredOrganizations: any[] = [];
    dialogRef: any;
    displayedColumns = ['avatar', 'name', 'city', 'adaptive', 'editbtn'];
    dataSource: MatTableDataSource<any>;
    FileUpload: Upload;
    filter: any = {
        name: '',
        country: '',
        city: '',
        state: ''
    };
    countries: any = [];
    filteredCountries: any = [];
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    constructor(public dialog: MatDialog,
                private uploadService: UploadService,
                private countryservice: CountryService) {
        this.getAllOrganizations();
        this.countryservice.getCountries().subscribe(
            resultArray => this.countries = resultArray,
            error => console.log('Error :: ' + error)
        );
    }

    ngOnInit() {

    }

    async getAllOrganizations(): Promise<void> {
        const query = firebase.firestore().collection('organizations');
        query.onSnapshot((snapshot) => {
            this.organizations = [];
            snapshot.forEach((doc) => {
                // this.clubs[doc.id] = doc.data();

                // firebase.firestore().collection('profiles').doc(doc.data().owner).get().then((userDoc) => {
                //     doc.data().userName = userDoc.data().nickName;
                // });
                this.organizations.push(doc.data());
            });

            this.dataSource = new MatTableDataSource(this.organizations);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
        }, (error) => {
            console.log(error);
        });
    }


    editOrganization(organization)
    {
        this.dialogRef = this.dialog.open(OrganizationFormComponent, {
            panelClass: 'contact-form-dialog',
            data      : {
                organization: organization,
                countries: this.countries,
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const formData: any = response[0];
                const editorganization: any = response[1];
                const logoimage: any = response[2];
                this.updateOrganization(editorganization, formData.value, logoimage);
            });
    }

    detailOrganization(organization)
    {
        this.dialogRef = this.dialog.open(OrganizationDetailComponent, {
            panelClass: 'contact-form-dialog',
            data      : {
                organization: organization,
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {

            });
    }

    async updateOrganization(organization, formdata, logo): Promise<void> {
        organization.date_update = new Date();
        organization.emails = formdata.emails;
        organization.phones = formdata.phones;
        organization.urls = formdata.urls;
        organization.adaptive.items = formdata.adaptive_items;
        await firebase.firestore().collection('organizations').doc(organization.id).update(Object.assign({}, organization));
        if (logo) {
            this.FileUpload = new Upload(logo);
            this.FileUpload.key = organization.id;
            organization.logo = await this.uploadService.pushFileToStorage(this.FileUpload, 'organizations', 'logo');

        }
    }

    async deleteOrganization(organization): Promise<void>
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                firebase.firestore().collection('organizations').doc(organization.id).delete();
                if (organization.logo) {
                    this.uploadService.deleteFileStorage(organization.logo.key, organization.logo.name, 'organizations');
                }
                if (organization.description) {
                    this.uploadService.deleteFileStorage(organization.description.key, organization.description.name, 'organizations');
                }
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(clubID)
    {
        // this.contactsService.toggleSelectedContact(contactId);
    }

    toggleStar(clubID)
    {
        // if ( this.user.starred.includes(contactId) )
        // {
        //     this.user.starred.splice(this.user.starred.indexOf(contactId), 1);
        // }
        // else
        // {
        //     this.user.starred.push(contactId);
        // }
        //
        // this.contactsService.updateUserData(this.user);
    }

    applyFilter(filterName: string, filterValue: string = null) {
        if (filterName === 'country') {
            this.filter.country = filterValue;
            this.filteredCountries = this.filterCountries(this.filter.country);
        } else if (filterName === 'city') {
            this.filter.city = filterValue;
        } else if (filterName === 'name') {
            this.filter.name = filterValue;
        } else if (filterName === 'state') {
            this.filter.state = filterValue;
        }

        this.filteredOrganizations = this.organizations.filter((organization) => {
            if (!organization.name) {
                organization.name = '';
            }
            if (!organization.country) {
                organization.country = '';
            }
            if (!organization.state) {
                organization.state = '';
            }
            if (!organization.city) {
                organization.city = '';
            }
            return (organization.name.toLowerCase().indexOf(this.filter.name.toLowerCase()) > -1 &&
                organization.country.toLowerCase().indexOf(this.filter.country.toLowerCase()) > -1 &&
                organization.state.toLowerCase().indexOf(this.filter.state.toLowerCase()) > -1 &&
                organization.city.toLowerCase().indexOf(this.filter.city.toLowerCase()) > -1);
        });
        this.dataSource = new MatTableDataSource(this.filteredOrganizations);
    }

    filterCountries(name: string) {
        return this.countries.filter(country =>
            country.Name.toLowerCase().indexOf(name.toLowerCase()) === 0);
    }
}

import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Organization } from '../../../st-models/organization.model';
import {FormControl} from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import { fuseAnimations } from '../../../core/animations';
import { WebCameraComponent } from '../../st-profile/web-camera/web-camera.component';

@Component({
  selector: 'fuse-organization-form',
  templateUrl: './organization-form.component.html',
  styleUrls: ['./organization-form.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
})
export class OrganizationFormComponent implements OnInit {

    event: CalendarEvent;
    dialogTitle: string;
    organizationForm: FormGroup;
    formErrors: any;
    action: string;
    organization: Organization;
    logoURL: any;
    selectedFiles: any;
    countries: any = [];
    filteredCountries: any = [];
    socialnetworks: any = [];
    sports$: Observable<any>;
    logodialogRef: any;
    constructor( public dialogRef: MatDialogRef<OrganizationFormComponent>,
                 @Inject(MAT_DIALOG_DATA) private data: any,
                 public dialog: MatDialog,
                 private formBuilder: FormBuilder,
                 private afs: AngularFirestore) {

        // Reactive form errors
        this.formErrors = {
            name: {},
            // owner_id: {},
            sports: {},
            emails: {},
            phones: {},
            urls: {},
            streetaddress1: {},
            streetaddress2: {},
            country: {},
            city: {},
            state: {},
            postalCode: {},
            adaptive_status: {},
            adaptive_items: {},
            adaptive_note: {},
            description: {}
        };

        // Reactive Form
        this.organizationForm = this.formBuilder.group({
            name: ['', Validators.required],
            sports: ['', Validators.required],
            urls: this.formBuilder.array([ this.createItem('url')]),
            emails: this.formBuilder.array([ this.createItem('email')]),
            phones: this.formBuilder.array([ this.createItem('phone')]),
            streetaddress1: ['', Validators.required],
            streetaddress2: ['', Validators.required],
            timezone: [''],
            city: ['', Validators.required],
            state: ['', Validators.required],
            postalCode: ['', [Validators.required, Validators.maxLength(5)]],
            country: ['', Validators.required],
            adaptive_status: [''],
            adaptive_items: this.formBuilder.array([ this.createItem('adapt')]),
            adaptive_note: [''],
            description: ['']
        });

        this.selectedFiles = null;
        this.logoURL = null;
        this.action = data.action;
        this.sports$ = this.afs.collection<any>('sports').valueChanges();

        if ( this.action === 'edit' )
        {
            this.dialogTitle = 'Edit Organization';
            this.organization = data.organization;
            this.countries = data.countries;
            this.filteredCountries = this.countries;
            if (this.organization.emails.length > 0) {
                for (let i = 0; i < this.organization.emails.length; i++) {
                    this.emails.push(this.formBuilder.group({
                        role: [this.organization.emails[i].role],
                        email: [this.organization.emails[i].email]
                    }));
                }
                this.emails.removeAt(0);
            }

            if (this.organization.phones.length > 0) {
                for (let i = 0; i < this.organization.phones.length; i++) {
                    this.phones.push(this.formBuilder.group({
                        role: [this.organization.phones[i].role],
                        phone: [this.organization.phones[i].phone]
                    }));
                }
                this.phones.removeAt(0);

            }

            if (this.organization.urls.length > 0) {
                for (let i = 0; i < this.organization.urls.length; i++) {
                    this.urls.push(this.formBuilder.group({
                        link: [this.organization.urls[i].link]
                    }));
                }
                this.urls.removeAt(0);

            }
            if (this.organization.adaptive.items.length > 0) {
                for (let i = 0; i < this.organization.adaptive.items.length; i++) {
                    this.adaptive_items.push(this.formBuilder.group({
                        name: [this.organization.adaptive.items[i].name],
                        status: [this.organization.adaptive.items[i].status],
                        value: [this.organization.adaptive.items[i].value]
                    }));
                }
                this.adaptive_items.removeAt(0);
            }

            if (this.organization.logo) {
                this.logoURL = this.organization.logo.url;
            }

        }
        else
        {
            this.dialogTitle = 'New organization';
            this.organization = new Organization();
            this.countries = data.countries;
            this.organization.country = 'United States';
            this.filteredCountries = this.countries;
        }
    }

    ngOnInit() {
        this.organizationForm.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
            if (this.organizationForm.value.country) {
                this.filteredCountries = this.filterCountries(this.organizationForm.value.country);
            }
        });
    }

    onFormValuesChanged()
    {
        for ( const field in this.formErrors )
        {
            if ( !this.formErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.organizationForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.formErrors[field] = control.errors;
            }
        }
    }

    createItem(item): FormGroup {
        if (item === 'url') {
            return this.formBuilder.group({
                link: ['']
            });
        }
        else if (item === 'phone') {
            return this.formBuilder.group({
                role: [''],
                phone: ['', Validators.required]
            });
        }
        else if (item === 'email') {
            return this.formBuilder.group({
                role: [''],
                email: ['', Validators.required]
            });
        }
        else if (item === 'adapt') {
            return this.formBuilder.group({
                name: [''],
                status: [''],
                value: ['']
            });
        }

    }

    get urls(): FormArray {
        return this.organizationForm.get('urls') as FormArray;
    }

    get phones(): FormArray {
        return this.organizationForm.get('phones') as FormArray;
    }

    get emails(): FormArray {
        return this.organizationForm.get('emails') as FormArray;
    }
    get adaptive_items(): FormArray {
        return this.organizationForm.get('adaptive_items') as FormArray;
    }

    deleteItem(item, index): void {
        if (index === 0) {
            return;
        }
        if (item === 'url') {
            this.urls.removeAt(index);
        }
        else if (item === 'phone') {
            this.phones.removeAt(index);
        }
        else if (item === 'email') {
            this.emails.removeAt(index);
        }
    }

    addItem(item): void {
        if (item === 'url') {
            this.urls.push(this.createItem(item));
        }
        else if (item === 'phone') {
            this.phones.push(this.createItem(item));
        }
        else if (item === 'email') {
            this.emails.push(this.createItem(item));
        }
    }

    async selectlogo(event): Promise<void> {
        this.selectedFiles = event.target.files.item(0);
        this.logoURL = await this.getBase64(event.target.files.item(0));
    }

    async getBase64(file): Promise<any> {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }

    applyFilter(value) {
        if (value) {
            this.filteredCountries = this.filterCountries(value);
        }
    }

    filterCountries(name: string) {
        return this.countries.filter(country =>
            country.Name.toLowerCase().indexOf(name.toLowerCase()) === 0);
    }

    cameracapture()
    {
        this.logodialogRef = this.dialog.open(WebCameraComponent, {
            panelClass: 'fuse-web-camera',
            data      : {
                profile: this.organization,
            },
            height: '600px',
            width: '300px',
        });

        this.logodialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }

                this.logoURL = response[0];
                this.selectedFiles = response[1];
            });
    }

}

import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Facility} from '../../../st-models/facility.model';
import { FacilityFormComponent } from '../facility-form/facility-form.component';
import { AngularFirestore } from 'angularfire2/firestore';
import { fuseAnimations } from '../../../core/animations';
import * as firebase from 'firebase';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import {UploadService} from '../../../st-services/upload/upload.service';
import {Upload} from '../../../st-models/upload.model';
import { CountryService } from '../../../st-services/country/country.service';
import 'moment';
declare let moment: any;

@Component({
  selector: 'fuse-facilty',
  templateUrl: './facilities.component.html',
  styleUrls: ['./facilities.component.scss'],
    providers: [CountryService],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
})
export class FacilitiesComponent implements OnInit {
    item: Observable<any>;
    facilities: any = [];
    imagepath: any;
    hasSelectedContacts: boolean;
    searchInput: FormControl;
    dialogRef: any;
    currentUserID: string;
    FileUpload: Upload;
    countries: any = [];

    constructor( private afs: AngularFirestore,
                 public dialog: MatDialog,
                 @Inject(SESSION_STORAGE) private storage: StorageService,
                 private uploadService: UploadService,
                 private countryservice: CountryService)
    {
        this.searchInput = new FormControl('');
        this.currentUserID = this.storage.get('uid');
        this.countryservice.getCountries().subscribe(
            resultArray => this.countries = resultArray,
            error => console.log('Error :: ' + error)
        );
    }

    ngOnInit() {

    }

    newFacility()
    {
        this.dialogRef = this.dialog.open(FacilityFormComponent, {
            panelClass: 'contact-form-dialog',
            data      : {
                countries: this.countries,
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if ( !response || !this.currentUserID )
                {
                    return;
                }
                const formData: any = response[0];
                const editfacility: any = response[1];
                const logoimage: any = response[2];
                const description: any = response[3];
                this.addNewFacility(formData.value, editfacility, logoimage, description);
            });

    }

    async addNewFacility(facility, editfacility, logo, description): Promise<void> {
        facility.date_create = new Date();
        facility.date_update = new Date();
        // set unique ID
        const clubid =
            moment().format('YYMMMDDTHHmmss') +
            '-' +
            Math.random()
                .toString(36)
                .slice(2);
        facility.id = clubid;
        facility.owner_id = this.currentUserID;
        firebase.firestore().collection('facilities').doc(clubid).set(Object.assign({}, facility));
        if (logo) {
            this.FileUpload = new Upload(logo);
            this.FileUpload.key = facility.id;
            facility.logo = await this.uploadService.pushFileToStorage(this.FileUpload, 'facilities', 'logo');

        }

        if (description) {
            this.FileUpload = new Upload(description);
            this.FileUpload.key = facility.id;
            facility.logo = await this.uploadService.pushFileToStorage(this.FileUpload, 'facilities', 'description');

        }
    }

}

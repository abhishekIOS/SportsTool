import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import * as firebase from 'firebase';

@Component({
  selector: 'fuse-facility-detail',
  templateUrl: './facility-detail.component.html',
  styleUrls: ['./facility-detail.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FacilityDetailComponent implements OnInit {

    facility: any;
    owner: any;
    constructor( public dialogRef: MatDialogRef<FacilityDetailComponent>,
                 @Inject(MAT_DIALOG_DATA) private data: any) {
        this.facility = data.facility;
        if (this.facility.owner_id) {
            firebase.firestore().collection('profiles').doc(this.facility.owner_id).get().then((doc) => {
                this.owner = doc.data();
            });

        }
    }

    ngOnInit() {
    }

}

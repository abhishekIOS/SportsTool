import { Component, OnInit, Input, ViewEncapsulation, ViewChild } from '@angular/core';
import {DataSource} from '@angular/cdk/collections';
import { FacilityFormComponent } from '../facility-form/facility-form.component';
import { MatDialog, MatDialogRef } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { fuseAnimations } from '../../../core/animations';
import { Subscription } from 'rxjs/Subscription';
import {MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import * as firebase from 'firebase';
import {UploadService} from '../../../st-services/upload/upload.service';
import { CountryService } from '../../../st-services/country/country.service';
import {Upload} from '../../../st-models/upload.model';
import { Club } from '../../../st-models/club.model';
import { FuseConfirmDialogComponent } from '../../../core/components/confirm-dialog/confirm-dialog.component';
import { FacilityDetailComponent } from '../facility-detail/facility-detail.component';
import {isUndefined} from 'util';

@Component({
  selector: 'fuse-facility-list',
  templateUrl: './facility-list.component.html',
  styleUrls: ['./facility-list.component.scss'],
    providers: [CountryService],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
})
export class FacilityListComponent implements OnInit {

    @Input() currentUserID: string;
    facilities: any[] = [];
    filteredClubs: any[] = [];
    dialogRef: any;
    displayedColumns = ['avatar', 'name', 'country', 'city', 'adaptive', 'editbtn'];
    dataSource: MatTableDataSource<any>;
    FileUpload: Upload;
    filter: any = {
        name: '',
        country: '',
        city: '',
        state: ''
    };
    countries: any = [];
    filteredCountries: any = [];
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    constructor(public dialog: MatDialog,
                private uploadService: UploadService,
                private countryservice: CountryService) {
        this.getAllFacilities();
        this.countryservice.getCountries().subscribe(
            resultArray => this.countries = resultArray,
            error => console.log('Error :: ' + error)
        );
    }

    ngOnInit() {

    }

    async getAllFacilities(): Promise<void> {
        const query = firebase.firestore().collection('facilities');
        query.onSnapshot((snapshot) => {
            this.facilities = [];
            snapshot.forEach((doc) => {
                // this.clubs[doc.id] = doc.data();

                // firebase.firestore().collection('profiles').doc(doc.data().owner).get().then((userDoc) => {
                //     doc.data().userName = userDoc.data().nickName;
                // });
                this.facilities.push(doc.data());
            });

            this.dataSource = new MatTableDataSource(this.facilities);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
        }, (error) => {
            console.log(error);
        });
    }


    editFacility(facility)
    {
        this.dialogRef = this.dialog.open(FacilityFormComponent, {
            panelClass: 'contact-form-dialog',
            data      : {
                facility: facility,
                countries: this.countries,
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }

                const formData: any = response[0];
                const editfacility: any = response[1];
                const logoimage: any = response[2];
                this.updateFacility(formData.value, editfacility, logoimage);
            });
    }

    detailFacility(facility)
    {
        this.dialogRef = this.dialog.open(FacilityDetailComponent, {
            panelClass: 'contact-form-dialog',
            data      : {
                facility: facility,
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {

            });
    }

    async updateFacility(formData, editfacility, logo): Promise<void> {
        editfacility.emails = formData.emails;
        editfacility.phones = formData.phones;
        editfacility.socialaccounts = formData.socialaccounts;
        editfacility.urls = formData.urls;
        editfacility.date_update = new Date();
        editfacility.adaptive.items = formData.adaptive_items;
        await firebase.firestore().collection('facilities').doc(editfacility.id).update(Object.assign({}, editfacility));
        if (logo) {
            this.FileUpload = new Upload(logo);
            this.FileUpload.key = editfacility.id;
            editfacility.logo = await this.uploadService.pushFileToStorage(this.FileUpload, 'facilities', 'logo');

        }
    }

    deleteFacility(facility)
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                firebase.firestore().collection('facilities').doc(facility.id).delete();
                if (facility.logo) {
                    this.uploadService.deleteFileStorage(facility.logo.key, facility.logo.name, 'facilities');
                }
                if (facility.description) {
                    this.uploadService.deleteFileStorage(facility.description.key, facility.description.name, 'facilities');
                }
            }
            this.confirmDialogRef = null;
        });

    }

    applyFilter(filterName: string, filterValue: string = null) {
        if (filterName === 'country') {
            this.filter.country = filterValue;
            this.filteredCountries = this.filterCountries(this.filter.country);
        } else if (filterName === 'city') {
            this.filter.city = filterValue;
        } else if (filterName === 'name') {
            this.filter.name = filterValue;
        } else if (filterName === 'state') {
            this.filter.state = filterValue;
        }

        this.filteredClubs = this.facilities.filter((club) => {
            if (!club.name) {
                club.name = '';
            }
            if (!club.country) {
                club.country = '';
            }
            if (!club.state) {
                club.state = '';
            }
            if (!club.city) {
                club.city = '';
            }
            return (club.name.toLowerCase().indexOf(this.filter.name.toLowerCase()) > -1 &&
                club.country.toLowerCase().indexOf(this.filter.country.toLowerCase()) > -1 &&
                club.state.toLowerCase().indexOf(this.filter.state.toLowerCase()) > -1 &&
                club.city.toLowerCase().indexOf(this.filter.city.toLowerCase()) > -1);
        });
        this.dataSource = new MatTableDataSource(this.filteredClubs);
    }

    filterCountries(name: string) {
        return this.countries.filter(country =>
            country.Name.toLowerCase().indexOf(name.toLowerCase()) === 0);
    }
}

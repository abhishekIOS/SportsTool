import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Facility } from '../../../st-models/facility.model';
import {FormControl} from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { AngularFirestore } from 'angularfire2/firestore';
import { MultiEmailComponent } from '../../st-profile/multi-email/multi-email.component';
import { MultiPhoneComponent } from '../../st-profile/multi-phone/multi-phone.component';
import { WebCameraComponent } from '../../st-profile/web-camera/web-camera.component';

@Component({
  selector: 'fuse-facility-form',
  templateUrl: './facility-form.component.html',
  styleUrls: ['./facility-form.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [MultiPhoneComponent, MultiEmailComponent]
})
export class FacilityFormComponent implements OnInit {

    dialogTitle: string;
    facilityForm: FormGroup;
    formErrors: any;
    action: string;
    facility: Facility;
    logoURL: any;
    selectedFiles: any;
    countries: any = [];
    filteredCountries: any = [];
    countryCtrl: FormControl;
    sports$: Observable<any>;
    logodialogRef: any;
    constructor( public dialogRef: MatDialogRef<FacilityFormComponent>,
                 @Inject(MAT_DIALOG_DATA) private data: any,
                 public dialog: MatDialog,
                 private formBuilder: FormBuilder,
                 private afs: AngularFirestore) {
        // Reactive form errors
        this.formErrors = {
            name: {},
            // owner_id: {},
            sports: {},
            emails: {},
            phones: {},
            urls: {},
            socialaccounts: {},
            streetaddress: {},
            country: {},
            city: {},
            state: {},
            postalCode: {},
            adaptive_status: {},
            adaptive_items: {},
            adaptive_note: {},
            description: {}
        };

        // Reactive Form
        this.facilityForm = this.formBuilder.group({
            name: ['', Validators.required],
            sports: ['', Validators.required],
            urls: this.formBuilder.array([ this.createItem('url')]),
            emails: this.formBuilder.array([ this.createItem('email')]),
            phones: this.formBuilder.array([ this.createItem('phone')]),
            socialaccounts: this.formBuilder.array([ this.createItem('social')]),
            streetaddress: ['', Validators.required],
            city: ['', Validators.required],
            state: ['', Validators.required],
            postalCode: ['', [Validators.required, Validators.maxLength(5)]],
            country: ['', Validators.required],
            adaptive_status: [''],
            adaptive_items: this.formBuilder.array([ this.createItem('adapt')]),
            adaptive_note: [''],
            description: ['']
        });

        this.selectedFiles = null;
        this.logoURL = null;
        this.action = data.action;
        this.countryCtrl = new FormControl();
        this.sports$ = this.afs.collection<any>('sports').valueChanges();
        if ( this.action === 'edit' )
        {
            this.dialogTitle = 'Edit Facility';
            this.facility = data.facility;
            this.countries = data.countries;
            this.filteredCountries = this.countries;
            if (this.facility.emails.length > 0) {
                for (let i = 0; i < this.facility.emails.length; i++) {
                    this.emails.push(this.formBuilder.group({
                        role: [this.facility.emails[i].role],
                        email: [this.facility.emails[i].email]
                    }));
                }
                this.emails.removeAt(0);
            }

            if (this.facility.phones.length > 0) {
                for (let i = 0; i < this.facility.phones.length; i++) {
                    this.phones.push(this.formBuilder.group({
                        role: [this.facility.phones[i].role],
                        phone: [this.facility.phones[i].phone]
                    }));
                }
                this.phones.removeAt(0);

            }

            if (this.facility.urls.length > 0) {
                for (let i = 0; i < this.facility.urls.length; i++) {
                    this.urls.push(this.formBuilder.group({
                        link: [this.facility.urls[i].link]
                    }));
                }
                this.urls.removeAt(0);

            }

            if (this.facility.socialaccounts.length > 0) {
                for (let i = 0; i < this.facility.socialaccounts.length; i++) {
                    this.socialaccounts.push(this.formBuilder.group({
                        social: [this.facility.socialaccounts[i].social],
                        id: [this.facility.socialaccounts[i].id]
                    }));
                }
                this.socialaccounts.removeAt(0);

            }

            if (this.facility.adaptive.items.length > 0) {
                for (let i = 0; i < this.facility.adaptive.items.length; i++) {
                    this.adaptive_items.push(this.formBuilder.group({
                        name: [this.facility.adaptive.items[i].name],
                        status: [this.facility.adaptive.items[i].status],
                        value: [this.facility.adaptive.items[i].value]
                    }));
                }
                this.adaptive_items.removeAt(0);
            }

            if (this.facility.logo) {
                this.logoURL = this.facility.logo.url;
            }
        }
        else
        {
            this.dialogTitle = 'New Facility';
            this.facility = new Facility();
            this.countries = data.countries;
            this.facility.country = 'United States';
            this.filteredCountries = this.countries;
        }

    }

    ngOnInit() {
        this.facilityForm.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
            if (this.facilityForm.value.country) {
                this.filteredCountries = this.filterCountries(this.facilityForm.value.country);
            }
        });
    }

    onFormValuesChanged()
    {
        for ( const field in this.formErrors )
        {
            if ( !this.formErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.facilityForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.formErrors[field] = control.errors;
            }
        }
    }

    createItem(item): FormGroup {
        if (item === 'social') {
            return this.formBuilder.group({
                social: [''],
                id: ['']
            });
        }

        else if (item === 'url') {
            return this.formBuilder.group({
                link: ['']
            });
        }
        else if (item === 'phone') {
            return this.formBuilder.group({
                role: [''],
                phone: ['', Validators.required]
            });
        }
        else if (item === 'email') {
            return this.formBuilder.group({
                role: [''],
                email: ['', Validators.required]
            });
        }
        else if (item === 'adapt') {
            return this.formBuilder.group({
                name: [''],
                status: [''],
                value: ['']
            });
        }

    }

    get socialaccounts(): FormArray {
        return this.facilityForm.get('socialaccounts') as FormArray;
    }

    get urls(): FormArray {
        return this.facilityForm.get('urls') as FormArray;
    }

    get phones(): FormArray {
        return this.facilityForm.get('phones') as FormArray;
    }

    get emails(): FormArray {
        return this.facilityForm.get('emails') as FormArray;
    }

    get adaptive_items(): FormArray {
        return this.facilityForm.get('adaptive_items') as FormArray;
    }
    deleteItem(item, index): void {
        if (index === 0) {
            return;
        }
        if (item === 'url') {
            this.urls.removeAt(index);
        }
        else if (item === 'social') {
            this.socialaccounts.removeAt(index);
        }
        else if (item === 'phone') {
            this.phones.removeAt(index);
        }
        else if (item === 'email') {
            this.emails.removeAt(index);
        }
    }

    addItem(item): void {
        if (item === 'url') {
            this.urls.push(this.createItem(item));
        }
        else if (item === 'social') {
            this.socialaccounts.push(this.createItem(item));
        }
        else if (item === 'phone') {
            this.phones.push(this.createItem(item));
        }
        else if (item === 'email') {
            this.emails.push(this.createItem(item));
        }
    }

    async selectlogo(event): Promise<void> {
        this.selectedFiles = event.target.files.item(0);
        this.logoURL = await this.getBase64(event.target.files.item(0));
    }

    async getBase64(file): Promise<any> {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }

    applyFilter(value) {
        if (value) {
            this.filteredCountries = this.filterCountries(value);
        }
    }

    filterCountries(name: string) {
        return this.countries.filter(country =>
            country.Name.toLowerCase().indexOf(name.toLowerCase()) === 0);
    }

    cameracapture()
    {
        this.logodialogRef = this.dialog.open(WebCameraComponent, {
            panelClass: 'fuse-web-camera',
            data      : {
                profile: this.facility,
            },
            height: '600px',
            width: '300px',
        });

        this.logodialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }

                this.logoURL = response[0];
                this.selectedFiles = response[1];
            });
    }

}

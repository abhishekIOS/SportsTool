import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { UploadService } from '../../../st-services/upload/upload.service';
import { Facility } from '../../../st-models/facility.model';
import { AngularFirestore } from 'angularfire2/firestore';
import { CountryService } from '../../../st-services/country/country.service';
import { Upload} from '../../../st-models/upload.model';
import { fuseAnimations } from '../../../core/animations';
import { Observable } from 'rxjs/Observable';
import 'moment';
declare let moment: any;
import * as _ from 'lodash';
import { MatDialog, MatDialogRef } from '@angular/material';
import { WebCameraComponent } from '../../st-profile/web-camera/web-camera.component';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import { MatSnackBar } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../core/components/confirm-dialog/confirm-dialog.component';
import { Router } from '@angular/router';
import * as firebase from 'firebase';

@Component({
  selector: 'fuse-new-facility',
  templateUrl: './new-facility.component.html',
  styleUrls: ['./new-facility.component.scss'],
    providers: [UploadService, CountryService],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
})
export class NewFacilityComponent implements OnInit {

    form: FormGroup;
    formErrors: any;
    sports$: Observable<any>;
    socialnetworks: any = [];
    countries: any = [];
    filteredCountries: any = [];
    newfacility: Facility;
    logoURL: any = null;
    dialogRef: any;
    selectedFiles: any;
    ref: any;
    FileUpload: Upload;
    disableBtn = false;
    currentUserID: string;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    constructor( private formBuilder: FormBuilder, public dialog: MatDialog,
                 private afs: AngularFirestore, private uploadService: UploadService,
                 @Inject(SESSION_STORAGE) private storage: StorageService,
                 public snackBar: MatSnackBar,
                 private countryservice: CountryService,
                 private router: Router) {

        // Reactive form errors
        this.formErrors = {
            name: {},
            // owner_id: {},
            sports: {},
            emails: {},
            phones: {},
            urls: {},
            socialaccounts: {},
            streetaddress: {},
            country: {},
            city: {},
            state: {},
            postalCode: {},
            adaptive_status: {},
            adaptive_items: {},
            adaptive_note: {},
            description: {}
        };

        // Reactive Form
        this.form = this.formBuilder.group({
            name: ['', Validators.required],
            sports: ['', Validators.required],
            urls: this.formBuilder.array([ this.createItem('url')]),
            emails: this.formBuilder.array([ this.createItem('email')]),
            phones: this.formBuilder.array([ this.createItem('phone')]),
            socialaccounts: this.formBuilder.array([ this.createItem('social')]),
            streetaddress: ['', Validators.required],
            city: ['', Validators.required],
            state: ['', Validators.required],
            postalCode: ['', [Validators.required, Validators.maxLength(5)]],
            country: ['', Validators.required],
            adaptive_status: [''],
            adaptive_items: this.formBuilder.array([ this.createItem('adapt')]),
            adaptive_note: [''],
            description: ['']
        });

        this.ref = firebase.firestore().collection('facilities');
        this.selectedFiles = null;
        this.countryservice.getCountries().subscribe(
            resultArray => this.countries = resultArray,
            error => console.log('Error :: ' + error)
        );
        this.newfacility = new Facility();
        this.currentUserID = this.storage.get('uid');
        // this will subscribe and console.log all the users in firestore upon
        this.sports$ = this.afs.collection<any>('sports').valueChanges();
        const query = firebase.firestore().collection('socialnetwork');
        query.onSnapshot((snapshot) => {
            this.socialnetworks = [];
            snapshot.forEach((doc) => {
                this.socialnetworks.push(doc.data());
            });
        }, (error) => {
            console.log(error);
        });

        firebase.firestore().collection('AdaptItems').onSnapshot((snapshot) => {
            snapshot.forEach((doc) => {
                this.adaptive_items.push(this.formBuilder.group({
                    name: [doc.data().name],
                    status: [false],
                    value: [1]
                }));
            });
            this.adaptive_items.removeAt(0);
        }, (error) => {
            console.log(error);
        });
    }

    async ngOnInit(): Promise<void> {

        await this.countryservice.getLocation().subscribe(
            result => this.setcountry(result.country),
            error => console.log('Error :: ' + error)
        );

        this.form.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
            if (this.form.value.country) {
                this.filteredCountries = this.filterCountries(this.form.value.country);
            }
        });
        if (!this.currentUserID) {
            this.confirmUser();
        }
    }

    confirmUser() {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: true
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Sorry, this action is needed login on site';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result ) {
                this.router.navigate(['/login']);
            } else {
                this.router.navigate(['/home']);
            }
            this.confirmDialogRef = null;
        });
    }

    setcountry(country) {
        if (country) {
            this.newfacility.country = country;
        } else {
            this.newfacility.country = 'United States';
        }
    }

    createItem(item): FormGroup {
        if (item === 'social') {
            return this.formBuilder.group({
                social: [''],
                id: ['']
            });
        }

        else if (item === 'url') {
            return this.formBuilder.group({
                link: ['']
            });
        }
        else if (item === 'phone') {
            return this.formBuilder.group({
                role: [''],
                phone: ['', Validators.required]
            });
        }
        else if (item === 'email') {
            return this.formBuilder.group({
                role: [''],
                email: ['', Validators.required]
            });
        }
        else if (item === 'adapt') {
            return this.formBuilder.group({
                name: [''],
                status: [''],
                value: ['']
            });
        }

    }

    get socialaccounts(): FormArray {
        return this.form.get('socialaccounts') as FormArray;
    }

    get urls(): FormArray {
        return this.form.get('urls') as FormArray;
    }

    get phones(): FormArray {
        return this.form.get('phones') as FormArray;
    }

    get emails(): FormArray {
        return this.form.get('emails') as FormArray;
    }

    get adaptive_items(): FormArray {
        return this.form.get('adaptive_items') as FormArray;
    }
    deleteItem(item, index): void {
        if (index === 0) {
            return;
        }
        if (item === 'url') {
            this.urls.removeAt(index);
        }
        else if (item === 'social') {
            this.socialaccounts.removeAt(index);
        }
        else if (item === 'phone') {
            this.phones.removeAt(index);
        }
        else if (item === 'email') {
            this.emails.removeAt(index);
        }
    }

    addItem(item): void {
        if (item === 'url') {
            this.urls.push(this.createItem(item));
        }
        else if (item === 'social') {
            this.socialaccounts.push(this.createItem(item));
        }
        else if (item === 'phone') {
            this.phones.push(this.createItem(item));
        }
        else if (item === 'email') {
            this.emails.push(this.createItem(item));
        }
    }

    filterCountries(name: string) {
        return this.countries.filter(country =>
            country.Name.toLowerCase().indexOf(name.toLowerCase()) === 0);
    }
    async onSubmit(): Promise<void> {
        if (this.form.invalid) {
            return;
        }
        this.disableBtn = true;
        const clubid =
            moment().format('YYMMMDDTHHmmss') +
            '-' +
            Math.random()
                .toString(36)
                .slice(2);
        this.newfacility.id = clubid;
        this.newfacility.owner_id = this.currentUserID;
        this.newfacility.date_create = new Date();
        this.newfacility.date_update = new Date();
        this.newfacility.emails = this.form.value.emails;
        this.newfacility.phones = this.form.value.phones;
        this.newfacility.socialaccounts = this.form.value.socialaccounts;
        this.newfacility.urls = this.form.value.urls;
        this.newfacility.adaptive.items = this.form.value.adaptive_items;
        this.newfacility.adaptive = Object.assign({}, this.newfacility.adaptive);

        const result = await this.addnewfacility(this.newfacility, this.selectedFiles);
        if (result) {
            this.disableBtn = false;
            this.snackBar.open('Add new facility success!', 'Dance', {
                duration: 3000
            });
        } else {
            this.disableBtn = false;
            this.snackBar.open('Add new facility failed!', 'Dance', {
                duration: 3000
            });
        }

    }

    async addnewfacility(club: any, logo: any): Promise<any>
    {
        for (const key of Object.keys(club)) {
            if ( club[key] === undefined) {
                club[key] = null;
            }
        }

        try {
            await this.ref.doc(club.id).set(Object.assign({}, club));
            if (logo) {
                this.FileUpload = new Upload(logo);
                this.FileUpload.key = club.id;
                await this.uploadService.pushFileToStorage(this.FileUpload, 'facilities', 'logo');
            }
            await console.log('Create New facility Success!: ');
            return true;
        } catch (error) {
            await console.log(error);
            return false;
        }
    }

    async selectlogo(event): Promise<void> {
        this.selectedFiles = event.target.files.item(0);
        this.logoURL = await this.getBase64(event.target.files.item(0));
    }

    async getBase64(file): Promise<any> {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }

    reset() {
        this.newfacility = new Facility();
    }

    onFormValuesChanged()
    {
        for ( const field in this.formErrors )
        {
            if ( !this.formErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.formErrors[field] = control.errors;
            }
        }
    }

    cameracapture()
    {
        this.dialogRef = this.dialog.open(WebCameraComponent, {
            panelClass: 'fuse-web-camera',
            data      : {
                profile: this.newfacility,
            },
            height: '600px',
            width: '300px',
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }

                this.logoURL = response[0];
                this.selectedFiles = response[1];
            });
    }
}


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FacilitiesComponent } from './facilities/facilities.component';
import { SharedModule } from '../../core/modules/shared.module';
import { FacilityDetailComponent } from './facility-detail/facility-detail.component';
import { FacilityListComponent } from './facility-list/facility-list.component';
import { FacilityFormComponent } from './facility-form/facility-form.component';
import { NewFacilityComponent } from './new-facility/new-facility.component';


const routes = [
  {
    path: '',
    component: FacilitiesComponent
  },
    {
        path     : 'newfacility',
        component: NewFacilityComponent
    },
    {
        path     : 'allfacilities',
        component: FacilitiesComponent
    }
];

@NgModule({
  imports: [
    CommonModule, 
    SharedModule,
    RouterModule.forChild(routes)],
  declarations: [FacilitiesComponent, FacilityDetailComponent, FacilityListComponent, FacilityFormComponent, NewFacilityComponent],
    entryComponents: [FacilityFormComponent, FacilityDetailComponent]
})
export class FacilitiesModule {}

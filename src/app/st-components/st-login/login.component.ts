import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FuseConfigService } from '../../core/services/config.service';
import { fuseAnimations } from '../../core/animations';
import { AuthService } from '../../st-services/auth/auth.service';
import { Router } from '@angular/router';
import { PhoneNumber } from '../../st-models/phonenumber.model';
import * as firebase from 'firebase';
import { ReCaptchaComponent } from 'angular2-recaptcha';
@Component({
    selector   : 'fuse-login',
    templateUrl: './login.component.html',
    styleUrls  : ['./login.component.scss'],
    providers: [AuthService],
    animations : fuseAnimations
})
export class FuseLoginComponent implements OnInit
{
    loginForm: FormGroup;
    loginFormErrors: any;

    isNewUser = true;
    email = null;
    password = null;
    errorMessage = '';
    error: { name: string, message: string } = { name: '', message: '' };

    resetPassword = false;
    checkBot = true;
    @ViewChild(ReCaptchaComponent) captcha: ReCaptchaComponent;
    constructor(
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        public authService: AuthService,
        private router: Router,
        public el: ElementRef)
    {
        this.fuseConfig.setSettings({
            layout: {
                navigation: 'none',
                toolbar   : 'none',
                footer    : 'none'
            }
        });

        this.loginFormErrors = {
            email   : {},
            password: {}
        };
    }

    resolved(captchaResponse: string) {
        console.log(`Resolved captcha with response ${captchaResponse}:`);
        if (captchaResponse) {
            this.checkBot = false;
        }

    }

    ngOnInit()
    {
        this.loginForm = this.formBuilder.group({
            email   : ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });

        this.loginForm.valueChanges.subscribe(() => {
            this.onLoginFormValuesChanged();
        });
    }

    resetForm() {
        this.captcha.reset();
        this.checkBot = true;
    }

    checkUserInfo() {
        if (this.authService.isUserEmailLoggedIn) {
            this.router.navigate(['/user']);
        }
    }

    clearErrorMessage() {
        this.errorMessage = '';
        this.error = { name: '', message: '' };
    }

    changeForm() {
        this.isNewUser = !this.isNewUser;
    }

    async onLoginEmail(): Promise<void> {
        this.clearErrorMessage();
        // firebase login section via loginWithEmail
        try {
            const result = await this.authService.loginWithEmail(this.email, this.password);
            if (result.uid) {
                this.router.navigate(['/']);
            }
            else if (result.code) {
                this.resetForm();
                this.errorMessage = result.message;
            }
        } catch (error) {
            this.resetForm();
            await console.log(error);
            this.errorMessage = error.message;
        }
    }

    async loginwithGoogle(): Promise<void>{
        try {
            const result = await this.authService.signInWithGoogle();
            if (result.uid) {
                this.router.navigate(['/']);
            }
            else if (result.code) {
                this.errorMessage = result.message;
                this.resetForm();
            }
        } catch (error) {
            this.resetForm();
            await console.log(error);
            this.errorMessage = error.message;
        }
    }

    async loginwithFacebook(): Promise<void>{
        try {
            const result = await this.authService.signInWithFacebook();
            if (result.uid) {
                this.router.navigate(['/']);
            }
            else if (result.code) {
                this.resetForm();
                this.errorMessage = result.message;
            }
        } catch (error) {
            this.resetForm();
            await console.log(error);
            this.errorMessage = error.message;
        }
    }

    sendResetEmail() {
        this.clearErrorMessage();

        // this.authService.resetPassword(this.email)
        //     .then(() => this.resetPassword = true)
        //     .catch(_error => {
        //         this.error = _error;
        //     })
    }

    onLoginFormValuesChanged()
    {
        for ( const field in this.loginFormErrors )
        {
            if ( !this.loginFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.loginFormErrors[field] = {};

            // Get the control
            const control = this.loginForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.loginFormErrors[field] = control.errors;
            }
        }
    }
}

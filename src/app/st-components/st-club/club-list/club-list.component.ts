import { Component, OnInit, Input, ViewEncapsulation, ViewChild } from '@angular/core';
import {DataSource} from '@angular/cdk/collections';
import { ClubFormComponent } from '../club-form/club-form.component';
import { MatDialog, MatDialogRef } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { ClubService } from '../shared/club.service';
import { fuseAnimations } from '../../../core/animations';
import { Subscription } from 'rxjs/Subscription';
import {MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import * as firebase from 'firebase';
import {UploadService} from '../../../st-services/upload/upload.service';
import { CountryService } from '../../../st-services/country/country.service';
import {Upload} from '../../../st-models/upload.model';
import { Club } from '../../../st-models/club.model';
import { FuseConfirmDialogComponent } from '../../../core/components/confirm-dialog/confirm-dialog.component';
import { ClubDetailComponent } from '../club-detail/club-detail.component';
import {isUndefined} from 'util';

@Component({
  selector: 'fuse-club-list',
  templateUrl: './club-list.component.html',
  styleUrls: ['./club-list.component.scss'],
    providers: [ClubService, CountryService],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
})
export class ClubListComponent implements OnInit {

    @Input() currentUserID: string;
    clubs: any[] = [];
    filteredClubs: any[] = [];
    dialogRef: any;
    displayedColumns = ['avatar', 'name', 'city', 'adaptive', 'editbtn'];
    dataSource: MatTableDataSource<any>;
    FileUpload: Upload;
    filter: any = {
        name: '',
        country: '',
        city: '',
        state: ''
    };
    countries: any = [];
    filteredCountries: any = [];
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
  constructor(private clubService: ClubService,
              public dialog: MatDialog,
              private uploadService: UploadService,
              private countryservice: CountryService) {
      this.getAllClubs();
      this.countryservice.getCountries().subscribe(
          resultArray => this.countries = resultArray,
          error => console.log('Error :: ' + error)
      );
  }

    ngOnInit() {

    }

    async getAllClubs(): Promise<void> {
        const query = firebase.firestore().collection('clubs');
        query.onSnapshot((snapshot) => {
                this.clubs = [];
                snapshot.forEach((doc) => {
                    // this.clubs[doc.id] = doc.data();

                    // firebase.firestore().collection('profiles').doc(doc.data().owner).get().then((userDoc) => {
                    //     doc.data().userName = userDoc.data().nickName;
                    // });
                    this.clubs.push(doc.data());
                });

                this.dataSource = new MatTableDataSource(this.clubs);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
            }, (error) => {
                console.log(error);
            });
    }


    editClub(club)
    {
        this.dialogRef = this.dialog.open(ClubFormComponent, {
            panelClass: 'contact-form-dialog',
            data      : {
                club: club,
                countries: this.countries,
                action : 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }
                const formData: any = response[0];
                const editclub: any = response[1];
                const logoimage: any = response[2];
                this.updateClub(editclub, formData.value, logoimage);
            });
    }

    detailClub(club)
    {
        this.dialogRef = this.dialog.open(ClubDetailComponent, {
            panelClass: 'contact-form-dialog',
            data      : {
                club: club,
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {

            });
    }

    async updateClub(club, formdata, logo): Promise<void> {
        club.date_update = new Date();
        club.emails = formdata.emails;
        club.phones = formdata.phones;
        club.socialaccounts = formdata.socialaccounts;
        club.urls = formdata.urls;
        club.adaptive.items = formdata.adaptive_items;
        await firebase.firestore().collection('clubs').doc(club.id).update(Object.assign({}, club));
        if (logo) {
            this.FileUpload = new Upload(logo);
            this.FileUpload.key = club.id;
            club.logo = await this.uploadService.pushFileToStorage(this.FileUpload, 'clubs', 'logo');

        }
    }

    async deleteClub(club): Promise<void>
    {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result )
            {
                firebase.firestore().collection('clubs').doc(club.id).delete();
                if (club.logo) {
                    this.uploadService.deleteFileStorage(club.logo.key, club.logo.name, 'clubs');
                }
                if (club.description) {
                    this.uploadService.deleteFileStorage(club.description.key, club.description.name, 'clubs');
                }
            }
            this.confirmDialogRef = null;
        });

    }

    onSelectedChange(clubID)
    {
        // this.contactsService.toggleSelectedContact(contactId);
    }

    toggleStar(clubID)
    {
        // if ( this.user.starred.includes(contactId) )
        // {
        //     this.user.starred.splice(this.user.starred.indexOf(contactId), 1);
        // }
        // else
        // {
        //     this.user.starred.push(contactId);
        // }
        //
        // this.contactsService.updateUserData(this.user);
    }

    applyFilter(filterName: string, filterValue: string = null) {
        if (filterName === 'country') {
            this.filter.country = filterValue;
            this.filteredCountries = this.filterCountries(this.filter.country);
        } else if (filterName === 'city') {
            this.filter.city = filterValue;
        } else if (filterName === 'name') {
            this.filter.name = filterValue;
        } else if (filterName === 'state') {
            this.filter.state = filterValue;
        }

        this.filteredClubs = this.clubs.filter((club) => {
            if (!club.name) {
                club.name = '';
            }
            if (!club.country) {
                club.country = '';
            }
            if (!club.state) {
                club.state = '';
            }
            if (!club.city) {
                club.city = '';
            }
            return (club.name.toLowerCase().indexOf(this.filter.name.toLowerCase()) > -1 &&
                club.country.toLowerCase().indexOf(this.filter.country.toLowerCase()) > -1 &&
                club.state.toLowerCase().indexOf(this.filter.state.toLowerCase()) > -1 &&
                club.city.toLowerCase().indexOf(this.filter.city.toLowerCase()) > -1);
            });
        this.dataSource = new MatTableDataSource(this.filteredClubs);
    }

    filterCountries(name: string) {
        return this.countries.filter(country =>
            country.Name.toLowerCase().indexOf(name.toLowerCase()) === 0);
    }
}


import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import * as firebase from 'firebase';

@Component({
  selector: 'fuse-club-detail',
  templateUrl: './club-detail.component.html',
  styleUrls: ['./club-detail.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ClubDetailComponent implements OnInit {

    club: any;
    owner: any;
  constructor( public dialogRef: MatDialogRef<ClubDetailComponent>,
               @Inject(MAT_DIALOG_DATA) private data: any) {
      this.club = data.club;
      if (this.club.owner_id) {
          firebase.firestore().collection('profiles').doc(this.club.owner_id).get().then((doc) => {
              this.owner = doc.data();
          });

      }
  }

  ngOnInit() {
  }

}

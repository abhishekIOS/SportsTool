import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ClubService } from '../shared/club.service';
import { Club} from '../../../st-models/club.model';
import { ClubFormComponent } from '../club-form/club-form.component';
import { AngularFirestore } from 'angularfire2/firestore';
import { fuseAnimations } from '../../../core/animations';
import * as firebase from 'firebase';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import {UploadService} from '../../../st-services/upload/upload.service';
import {Upload} from '../../../st-models/upload.model';
import { CountryService } from '../../../st-services/country/country.service';
import 'moment';
declare let moment: any;

@Component({
  selector: 'fuse-club',
  templateUrl: './club.component.html',
  styleUrls: ['./club.component.scss'],
    providers: [ClubService, CountryService],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
})
export class ClubComponent implements OnInit {
    item: Observable<any>;
    clubs: any = [];
    imagepath: any;
    hasSelectedContacts: boolean;
    searchInput: FormControl;
    dialogRef: any;
    currentUserID: string;
    FileUpload: Upload;
    countries: any = [];

    constructor( private afs: AngularFirestore, private clubService: ClubService,
                public dialog: MatDialog,
                 @Inject(SESSION_STORAGE) private storage: StorageService,
                 private uploadService: UploadService,
                 private countryservice: CountryService)
    {
        this.searchInput = new FormControl('');
        this.currentUserID = this.storage.get('uid');
        this.countryservice.getCountries().subscribe(
            resultArray => this.countries = resultArray,
            error => console.log('Error :: ' + error)
        );
  }

  ngOnInit() {
      // this.searchInput.valueChanges
      //     .debounceTime(300)
      //     .distinctUntilChanged()
      //     .subscribe(searchText => {
      //         this.clubService.onSearchTextChanged.next(searchText);
      //     });
  }

    newClub()
    {
        this.dialogRef = this.dialog.open(ClubFormComponent, {
            panelClass: 'contact-form-dialog',
            data      : {
                countries: this.countries,
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if ( !response || !this.currentUserID )
                {
                    return;
                }
                const formData: any = response[0];
                const editclub: any = response[1];
                const logoimage: any = response[2];
                const description: any = response[3];
                this.addNewClub(formData.value, editclub, logoimage, description);
            });

    }

    async addNewClub(club, editclub, logo, description): Promise<void> {
        // set unique ID
        const clubid =
            moment().format('YYMMMDDTHHmmss') +
            '-' +
            Math.random()
                .toString(36)
                .slice(2);
        club.id = clubid;
        club.owner_id = this.currentUserID;
        club.date_create = new Date();
        club.date_update = new Date();
        await firebase.firestore().collection('clubs').doc(clubid).set(Object.assign({}, club));
        if (logo) {
            this.FileUpload = new Upload(logo);
            this.FileUpload.key = club.id;
            club.logo = await this.uploadService.pushFileToStorage(this.FileUpload, 'clubs', 'logo');

        }

        if (description) {
            this.FileUpload = new Upload(description);
            this.FileUpload.key = club.id;
            club.logo = await this.uploadService.pushFileToStorage(this.FileUpload, 'clubs', 'description');

        }
    }
}

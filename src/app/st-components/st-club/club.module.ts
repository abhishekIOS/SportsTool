import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ClubComponent } from './club/club.component';
import { ClubFormComponent } from './club-form/club-form.component';
import { SharedModule } from '../../core/modules/shared.module';
import { ClubListComponent } from './club-list/club-list.component';
import { ClubDetailComponent } from './club-detail/club-detail.component';
import { NewClubComponent } from './new-club/new-club.component';

const routes = [
  {
    path: '',
    component: ClubComponent
  },
    {
        path     : 'newclub',
        component: NewClubComponent
    },
    {
        path     : 'allclubs',
        component: ClubComponent
    }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)],
  declarations: [ClubComponent,
      ClubFormComponent,
      ClubListComponent,
      ClubDetailComponent,
      NewClubComponent],
  entryComponents: [ClubFormComponent, ClubDetailComponent]

})
export class ClubModule {}

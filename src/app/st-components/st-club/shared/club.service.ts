import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import { Club} from '../../../st-models/club.model';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FuseUtils } from '../../../core/fuseUtils';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ClubService {

    clubs: any[] = [];
    club: any;
    prodcollection: AngularFirestoreCollection<any>;
    reference: any;
    onClubsChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onSelectedContactsChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onUserDataChanged: BehaviorSubject<any> = new BehaviorSubject([]);
    onSearchTextChanged: Subject<any> = new Subject();
    onFilterChanged: Subject<any> = new Subject();
    selectedContacts: string[] = [];

    searchText: string;
    filterBy: string;

    constructor( private afs: AngularFirestore ) {
        this.reference = firebase.firestore().collection('clubs');
        this.clubs = [];
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getAllData()
            ]).then(
                ([files]) => {

                    resolve();

                },
                reject
            );
        });
    }

    // getAllData(): Promise<any>{
    //     return new Promise((resolve, reject) => {
    //         this.reference.get().then((querySnapshot: any) => {
    //             // querySnapshot.forEach((doc) => {
    //             //     this.clubs.push(doc.data());
    //             // });
    //             // get the data of all the documents into an array
    //             querySnapshot.docs.map( (documentSnapshot) => {
    //                 this.clubs = documentSnapshot.data();
    //             console.log('service', this.clubs);
    //
    //             });
    //             this.onClubsChanged.next(this.clubs);
    //
    //             resolve();
    //         }, reject);
    //         }
    //     );
    getAllData() {
        // return this.afs.collection('clubs', ref => ref).valueChanges();



        return this.afs.collection('clubs').valueChanges();
        // return this.reference.get().then((resp) => {
        //     resp.forEach((doc) => {
        //         this.clubs.push(doc.data());
        //     });
        //     return this.clubs;
        // });
        // return new Promise((resolve, reject) => {
        //     this.reference.get().then((querySnapshot: any) => {
        //         // resp.forEach((doc) => {
        //         //     this.clubs.push(doc.data());
        //         // });
        //         // get the data of all the documents into an array
        //         querySnapshot.docs.map( (documentSnapshot) => {
        //             return documentSnapshot.data();
        //         });
        // //         console.log(this.clubs)
        // //         this.onClubsChanged.next(this.clubs);
        // //         resolve(this.clubs);
        //         }, reject);
        //     }
        // );
    }

    getItem(key: string): any {
        // const itemPath =  `${'users'}/${key}`;
        // this.user = this.firebase.object(itemPath);
        // return this.user;
    }

    insertItem(club: Club)
    {
        // this.db.doc(club.id).set(club)
        //     .then(() => {
        //         console.log('Document written Success!: ');
        //     })
        //     .catch((err) => {
        //         console.log(err);
        //     });
    }

    deleteUser(club) {
        this.prodcollection.doc(club.id).delete().then(() => {
            console.log('deleted');
        });
    }

}

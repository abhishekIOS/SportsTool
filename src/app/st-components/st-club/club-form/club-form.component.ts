import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Club } from '../../../st-models/club.model';
import {FormControl} from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import { fuseAnimations } from '../../../core/animations';
import { WebCameraComponent } from '../../st-profile/web-camera/web-camera.component';

@Component({
  selector: 'fuse-club-form-dialog',
  templateUrl: './club-form.component.html',
  styleUrls: ['./club-form.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
})
export class ClubFormComponent implements OnInit {

    event: CalendarEvent;
    dialogTitle: string;
    clubForm: FormGroup;
    formErrors: any;
    action: string;
    club: Club;
    logoURL: any;
    selectedFiles: any;
    countries: any = [];
    filteredCountries: any = [];
    socialnetworks: any = [];
    sports$: Observable<any>;
    logodialogRef: any;
  constructor( public dialogRef: MatDialogRef<ClubFormComponent>,
               @Inject(MAT_DIALOG_DATA) private data: any,
               public dialog: MatDialog,
               private formBuilder: FormBuilder,
               private afs: AngularFirestore) {

      // Reactive form errors
      this.formErrors = {
          name: {},
          // owner_id: {},
          sports: {},
          emails: {},
          phones: {},
          urls: {},
          socialaccounts: {},
          streetaddress: {},
          country: {},
          city: {},
          state: {},
          postalCode: {},
          adaptive_status: {},
          adaptive_items: {},
          adaptive_note: {},
          description: {}
      };

      // Reactive Form
      this.clubForm = this.formBuilder.group({
          name: ['', Validators.required],
          sports: ['', Validators.required],
          urls: this.formBuilder.array([ this.createItem('url')]),
          emails: this.formBuilder.array([ this.createItem('email')]),
          phones: this.formBuilder.array([ this.createItem('phone')]),
          socialaccounts: this.formBuilder.array([ this.createItem('social')]),
          streetaddress: ['', Validators.required],
          city: ['', Validators.required],
          state: ['', Validators.required],
          postalCode: ['', [Validators.required, Validators.maxLength(5)]],
          country: ['', Validators.required],
          adaptive_status: [''],
          adaptive_items: this.formBuilder.array([ this.createItem('adapt')]),
          adaptive_note: [''],
          description: ['']
      });

      this.selectedFiles = null;
      this.logoURL = null;
      this.action = data.action;
      this.sports$ = this.afs.collection<any>('sports').valueChanges();
      const query = firebase.firestore().collection('socialnetwork');
      query.onSnapshot((snapshot) => {
          this.socialnetworks = [];
          snapshot.forEach((doc) => {
              this.socialnetworks.push(doc.data());
          });
      }, (error) => {
          console.log(error);
      });
      if ( this.action === 'edit' )
      {
          this.dialogTitle = 'Edit Club';
          this.club = data.club;
          this.countries = data.countries;
          this.filteredCountries = this.countries;
          if (this.club.emails.length > 0) {
              for (let i = 0; i < this.club.emails.length; i++) {
                  this.emails.push(this.formBuilder.group({
                      role: [this.club.emails[i].role],
                      email: [this.club.emails[i].email]
                  }));
              }
              this.emails.removeAt(0);
          }

          if (this.club.phones.length > 0) {
              for (let i = 0; i < this.club.phones.length; i++) {
                  this.phones.push(this.formBuilder.group({
                      role: [this.club.phones[i].role],
                      phone: [this.club.phones[i].phone]
                  }));
              }
              this.phones.removeAt(0);

          }

          if (this.club.urls.length > 0) {
              for (let i = 0; i < this.club.urls.length; i++) {
                  this.urls.push(this.formBuilder.group({
                      link: [this.club.urls[i].link]
                  }));
              }
              this.urls.removeAt(0);

          }

          if (this.club.socialaccounts.length > 0) {
              for (let i = 0; i < this.club.socialaccounts.length; i++) {
                  this.socialaccounts.push(this.formBuilder.group({
                      social: [this.club.socialaccounts[i].social],
                      id: [this.club.socialaccounts[i].id]
                  }));
              }
              this.socialaccounts.removeAt(0);

          }

          if (this.club.adaptive.items.length > 0) {
              for (let i = 0; i < this.club.adaptive.items.length; i++) {
                  this.adaptive_items.push(this.formBuilder.group({
                      name: [this.club.adaptive.items[i].name],
                      status: [this.club.adaptive.items[i].status],
                      value: [this.club.adaptive.items[i].value]
                  }));
              }
              this.adaptive_items.removeAt(0);
          }

          if (this.club.logo) {
              this.logoURL = this.club.logo.url;
          }

      }
      else
      {
          this.dialogTitle = 'New Club';
          this.club = new Club();
          this.countries = data.countries;
          this.club.country = 'United States';
          this.filteredCountries = this.countries;
      }
  }

  ngOnInit() {
      this.clubForm.valueChanges.subscribe(() => {
          this.onFormValuesChanged();
          if (this.clubForm.value.country) {
              this.filteredCountries = this.filterCountries(this.clubForm.value.country);
          }
      });
  }

    onFormValuesChanged()
    {
        for ( const field in this.formErrors )
        {
            if ( !this.formErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.clubForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.formErrors[field] = control.errors;
            }
        }
    }

    createItem(item): FormGroup {
        if (item === 'social') {
            return this.formBuilder.group({
                social: [''],
                id: ['']
            });
        }

        else if (item === 'url') {
            return this.formBuilder.group({
                link: ['']
            });
        }
        else if (item === 'phone') {
            return this.formBuilder.group({
                role: [''],
                phone: ['', Validators.required]
            });
        }
        else if (item === 'email') {
            return this.formBuilder.group({
                role: [''],
                email: ['', Validators.required]
            });
        }
        else if (item === 'adapt') {
            return this.formBuilder.group({
                name: [''],
                status: [''],
                value: ['']
            });
        }

    }

    get socialaccounts(): FormArray {
        return this.clubForm.get('socialaccounts') as FormArray;
    }

    get urls(): FormArray {
        return this.clubForm.get('urls') as FormArray;
    }

    get phones(): FormArray {
        return this.clubForm.get('phones') as FormArray;
    }

    get emails(): FormArray {
        return this.clubForm.get('emails') as FormArray;
    }
    get adaptive_items(): FormArray {
        return this.clubForm.get('adaptive_items') as FormArray;
    }

    deleteItem(item, index): void {
        if (index === 0) {
            return;
        }
        if (item === 'url') {
            this.urls.removeAt(index);
        }
        else if (item === 'social') {
            this.socialaccounts.removeAt(index);
        }
        else if (item === 'phone') {
            this.phones.removeAt(index);
        }
        else if (item === 'email') {
            this.emails.removeAt(index);
        }
    }

    addItem(item): void {
        if (item === 'url') {
            this.urls.push(this.createItem(item));
        }
        else if (item === 'social') {
            this.socialaccounts.push(this.createItem(item));
        }
        else if (item === 'phone') {
            this.phones.push(this.createItem(item));
        }
        else if (item === 'email') {
            this.emails.push(this.createItem(item));
        }
    }

    async selectlogo(event): Promise<void> {
        this.selectedFiles = event.target.files.item(0);
        this.logoURL = await this.getBase64(event.target.files.item(0));
    }

    async getBase64(file): Promise<any> {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }

    applyFilter(value) {
      if (value) {
          this.filteredCountries = this.filterCountries(value);
      }
    }

    filterCountries(name: string) {
        return this.countries.filter(country =>
            country.Name.toLowerCase().indexOf(name.toLowerCase()) === 0);
    }
    cameracapture()
    {
        this.logodialogRef = this.dialog.open(WebCameraComponent, {
            panelClass: 'fuse-web-camera',
            data      : {
                profile: this.club,
            },
            height: '600px',
            width: '300px',
        });

        this.logodialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }

                this.logoURL = response[0];
                this.selectedFiles = response[1];
            });
    }

}

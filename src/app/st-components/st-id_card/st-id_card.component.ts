import { Component, OnDestroy, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { ProjectsDashboardService } from './st-id_card.service';
import * as shape from 'd3-shape';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { DataSource } from '@angular/cdk/collections';
import { fuseAnimations } from '../../core/animations';
import * as firebase from 'firebase';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import { CountryService } from '../../st-services/country/country.service';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';


@Component({
    selector: 'fuse-project',
    templateUrl: './st-id_card.component.html',
    styleUrls: ['./st-id_card.component.scss'],
    providers: [ CountryService ],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class FuseProjectComponent implements OnInit, OnDestroy {
    projects: any[];
    selectedProject: any;
    status = false;
    user$: Observable<any>;
    countries: any = [];
    countrycode: any = null;

    widgets: any;
    widget5: any = {};
    widget6: any = {};
    widget7: any = {};
    widget8: any = {};
    widget9: any = {};
    widget11: any = {};

    user: any;
    dateNow = Date.now();

    constructor(private projectsDashboardService: ProjectsDashboardService,
                private afs: AngularFirestore,
                @Inject(SESSION_STORAGE) private storage: StorageService,
                private countryservice: CountryService) {

        // code below get the user IDs from FireStore
        this.user$  = this.afs.collection<any>('profiles').snapshotChanges().map(actions => {
            return actions.map(a => {
                return a.payload.doc.id;
            });
        });




        this.projects = this.projectsDashboardService.projects;

        this.selectedProject = this.projects[0];

        this.widgets = this.projectsDashboardService.widgets;

        /**
         * Widget 5
         */
        this.widget5 = {
            currentRange: 'TW',
            xAxis: true,
            yAxis: true,
            gradient: false,
            legend: false,
            showXAxisLabel: false,
            xAxisLabel: 'Days',
            showYAxisLabel: false,
            yAxisLabel: 'Isues',
            scheme: {
                domain: ['#42BFF7', '#C6ECFD', '#C7B42C', '#AAAAAA']
            },
            onSelect: (ev) => {
                console.log(ev);
            },
            supporting: {
                currentRange: '',
                xAxis: false,
                yAxis: false,
                gradient: false,
                legend: false,
                showXAxisLabel: false,
                xAxisLabel: 'Days',
                showYAxisLabel: false,
                yAxisLabel: 'Isues',
                scheme: {
                    domain: ['#42BFF7', '#C6ECFD', '#C7B42C', '#AAAAAA']
                },
                curve: shape.curveBasis
            }
        };

        /**
         * Widget 6
         */
        this.widget6 = {
            currentRange: 'TW',
            legend: false,
            explodeSlices: false,
            labels: true,
            doughnut: true,
            gradient: false,
            scheme: {
                domain: ['#f44336', '#9c27b0', '#03a9f4', '#e91e63']
            },
            onSelect: (ev) => {
                console.log(ev);
            }
        };

        /**
         * Widget 7
         */
        this.widget7 = {
            currentRange: 'T'
        };

        /**
         * Widget 8
         */
        this.widget8 = {
            legend: false,
            explodeSlices: false,
            labels: true,
            doughnut: false,
            gradient: false,
            scheme: {
                domain: ['#f44336', '#9c27b0', '#03a9f4', '#e91e63', '#ffc107']
            },
            onSelect: (ev) => {
                console.log(ev);
            }
        };

        /**
         * Widget 9
         */
        this.widget9 = {
            currentRange: 'TW',
            xAxis: false,
            yAxis: false,
            gradient: false,
            legend: false,
            showXAxisLabel: false,
            xAxisLabel: 'Days',
            showYAxisLabel: false,
            yAxisLabel: 'Isues',
            scheme: {
                domain: ['#42BFF7', '#C6ECFD', '#C7B42C', '#AAAAAA']
            },
            curve: shape.curveBasis
        };

        setInterval(() => {
            this.dateNow = Date.now();
        }, 1000);

    }

    async ngOnInit() {
        /**
         * Widget 11
         */
        this.widget11.onContactsChanged = new BehaviorSubject({});
        this.widget11.onContactsChanged.next(this.widgets.widget11.table.rows);
        this.widget11.dataSource = new FilesDataSource(this.widget11);

        await this.countryservice.getCountries().subscribe(
            resultArray => this.countries = resultArray,
            error => console.log('Error :: ' + error)
        );

        // get current user with signed user
        const userid = this.storage.get('uid');
        if (userid) {
            firebase.firestore().collection('profiles').doc(userid).get()
                .then((doc) => {
                    this.user = doc.data();
                    if (this.user.picture) {
                        this.status = true;
                    } else {
                        this.status = false;
                    }

                    if (this.user.countryOfBirth) {
                        if (this.countries.find(c => c.Name === this.user.countryOfBirth)) {
                            this.countrycode = this.countries.find(c => c.Name === this.user.countryOfBirth).Code.toLowerCase();
                        }
                    }
                });
        }
    }

    // render(e){
    //     console.log('hhhhh', e.result);
    // }
    //
    // decodedOutput($event: string) {
    //     console.log('Decoded', $event);
    // }
    //
    // listCameras($event: MediaDeviceInfo[]) {
    //     console.log('MediaDeviceInfo', $event);
    //     this.chosenCameraSubject.next($event.filter(device => device.kind === 'videoinput')[0]);
    // }
    ngOnDestroy() {
    }

}

export class FilesDataSource extends DataSource<any>
{
    constructor(private widget11) {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]> {
        return this.widget11.onContactsChanged;
    }

    disconnect() {
    }
}


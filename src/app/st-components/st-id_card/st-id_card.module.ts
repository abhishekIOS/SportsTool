import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FuseProjectComponent } from './st-id_card.component';
import { SharedModule } from '../../core/modules/shared.module';
import { ProjectsDashboardService } from './st-id_card.service';
import { FuseWidgetModule } from '../../core/components/widget/widget.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { QRCodeModule } from 'angular2-qrcode';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgQRCodeReaderModule } from 'ng2-qrcode-reader';
import { NgQrScannerModule } from 'angular2-qrscanner';

const routes: Routes = [
    {
        path: '**',
        component: FuseProjectComponent,
        resolve: {
            data: ProjectsDashboardService
        }
    }
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        FuseWidgetModule,
        NgxChartsModule,
        QRCodeModule,
        NgxBarcodeModule,
        NgQRCodeReaderModule,
        NgQrScannerModule
    ],
    declarations: [
        FuseProjectComponent
    ],
    providers: [
        ProjectsDashboardService
    ]
})
export class ID_CardModule {
}


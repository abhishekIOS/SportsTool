import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
// import { FuseToolbarComponent } from '../main/toolbar/toolbar.component';
import 'hammerjs';
import { TranslateModule } from '@ngx-translate/core';
const appRoutes: Routes = [
    {
        path: 'login',
        loadChildren: './st-login/login.module#LoginModule'
    },
    {
        path: 'register',
        loadChildren: './st-register/st-register.module#RegisterModule'
    },
    {
        path: 'forgot-password',
        loadChildren: './st-forgot-passwd/forgot-passwd.module#ForgotPasswordModule'
    },
    {
        path: 'home',
        loadChildren: './st-home/home.module#HomeModule'
    },
    {
        path: 'users',
        loadChildren: './st-users/st-users.module#StUsersModule'
    },
    {
        path: 'facilities',
        loadChildren: './st-facilities/facilities.module#FacilitiesModule'
    },
    {
        path: 'club',
        loadChildren: './st-club/club.module#ClubModule'
    },
    {
        path: 'team',
        loadChildren: './st-team/team.module#TeamModule'
    },
    {
        path: 'organization',
        loadChildren: './st-organization/organization.module#OrganizationModule'
    },
    {
        path: 'create_schedule',
        loadChildren: './st-create-schedule/create-schedule.module#CreateScheduleModule'
    },
    {
        path: 'profile',
        loadChildren: './st-profile/profile.module#ProfileModule'
    },
    {
        path: 'id_card',
        loadChildren: './st-id_card/st-id_card.module#ID_CardModule'
    }
];

@NgModule({
    declarations: [
        // FuseToolbarComponent,
        ],
    imports: [
        BrowserModule,
        HttpClientModule,
        BrowserAnimationsModule,
        RouterModule.forRoot(appRoutes),
    ],
    exports: [
        RouterModule
    ],
    providers: []
})
export class STAppModule {
}

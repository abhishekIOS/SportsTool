import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../../st-services/auth/auth.service';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import * as firebase from 'firebase';
import { CountryService } from '../../../st-services/country/country.service';
import { LocationInfo} from '../../../st-models/locationInfo';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'moment';
declare let moment: any;

@Component({
  selector: 'fuse-home',
  templateUrl: './home.component.html',
  providers: [CountryService],
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  form: FormGroup;
  formErrors: any;
  currentuser: any;
  locationInfo: LocationInfo;
  users: any[] = [];
  teams: any[] = [];
  organizations: any[] = [];
  clubs: any[] = [];
  sports: any[] = [];

  private itemDoc: AngularFirestoreDocument<any>;
  item: Observable<any>;
  user = {
    firstName: '',
    lastName: '',
    nickName: '',
    gender: '',
    birthday: '',
    cityOfBirth: '',
    countryOfBirth: '',
    age: 0,
    sports: '',
    email: '',
    cellPhone: '',
    homePhone: '',
    role: '',
    address: '',
    address2: '',
    city: '',
    state: '',
    postalCode: ''
  };

  constructor(private formBuilder: FormBuilder, private afs: AngularFirestore,
              public authService: AuthService,
              @Inject(SESSION_STORAGE) private storage: StorageService,
              private countryservice: CountryService) {
    // Reactive form errors
    this.formErrors = {
      firstName: {},
      lastName: {},
      nickName: {},
      gender: {},
      birthday: {},
      cityOfBirth: {},
      countryOfBirth: {},
      sports: {},
      email: {},
      cellPhone: {},
      homePhone: {},
      role: {},
      address: {},
      address2: {},
      city: {},
      state: {},
      postalCode: {},
      country: {},
      adaptiveNeeds: {}
    };

      // get current user with signed user
      const userid = this.storage.get('uid');
      if (userid) {
          // -------not realtime data binding, only get data---------//
          // firebase.firestore().collection('profiles').doc(userid).get()
          //     .then((doc) => {
          //         this.currentuser = doc.data();
          //     });

          // -------realtime data binding---------//
          firebase.firestore().collection('profiles').doc(userid).onSnapshot
          ({includeMetadataChanges: true}, (doc) => {
              this.currentuser = doc.data();
          });
      } else {
        this.currentuser = null;
      }

      this.locationInfo = new LocationInfo();
      this.countryservice.getLocation().subscribe(
          result => this.locationInfo = result,
          error => console.log('Error :: ' + error)
      );
  }

  ngOnInit() {
    // Reactive Form
    this.form = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      nickName: [''],
      gender: ['', Validators.required],
      birthday: ['', Validators.required],
      cityOfBirth: [''],
      countryOfBirth: [''],
      sports: [''],
      email: ['', Validators.required],
      cellPhone: ['', Validators.required],
      homePhone: [''],
      role: ['', Validators.required],
      address: ['', Validators.required],
      address2: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      postalCode: ['', [Validators.required, Validators.maxLength(5)]],
      country: ['', Validators.required],
      adaptiveNeeds: ['']
    });

      firebase.firestore().collection('teams').onSnapshot((snapshot) => {
          this.teams = [];
          snapshot.forEach((doc) => {
              this.teams.push(doc.data());
          });
      }, (error) => {
          console.log(error);
      });
      firebase.firestore().collection('profiles').onSnapshot((snapshot) => {
          this.users = [];
          snapshot.forEach((doc) => {
              this.users.push(doc.data());
          });
      }, (error) => {
          console.log(error);
      });
      firebase.firestore().collection('clubs').onSnapshot((snapshot) => {
          this.clubs = [];
          snapshot.forEach((doc) => {
              this.clubs.push(doc.data());
          });
      }, (error) => {
          console.log(error);
      });
      firebase.firestore().collection('sports').onSnapshot((snapshot) => {
          this.sports = [];
          snapshot.forEach((doc) => {
              this.sports.push(doc.data());
          });
      }, (error) => {
          console.log(error);
      });
      firebase.firestore().collection('organizations').onSnapshot((snapshot) => {
          this.organizations = [];
          snapshot.forEach((doc) => {
              this.organizations.push(doc.data());
          });
      }, (error) => {
          console.log(error);
      });

    const date = new Date();
    // console.log(this.form.value.birthday);
    this.form.valueChanges.subscribe(() => {
      this.onFormValuesChanged();
      if (this.form.value.birthday) {
        const bday = moment(this.form.value.birthday);
        this.user.age = bday;
      }
    });
  }

  saveFB() {
    // taking the form value and putting it into profile. This makes it more managable.
    const profile = this.form.value;
    // this creates an ID with this format: '18Jan28T220328-z1i9ym5sqq'
    profile.id =
      moment().format('YYMMMDDTHHmmss') +
      '-' +
      Math.random()
        .toString(36)
        .slice(2);
    // below let you set your own ID
    this.afs.doc<any>(`users/${profile.id}`).set(profile);
    // below let firestore generate ID
    this.afs.collection<any>(`users`).add(profile);
  }

  deleteFB() {
    const profile = this.form.value;
    profile.id = '123456';
    // below lets you delete a document. You need to provide the ID.
    this.afs.doc<any>(`users/${profile.id}`).delete();
  }

  onFormValuesChanged() {
    for (const field in this.formErrors) {
      if (!this.formErrors.hasOwnProperty(field)) {
        continue;
      }

      // Clear previous errors
      this.formErrors[field] = {};

      // Get the control
      const control = this.form.get(field);

      if (control && control.dirty && !control.valid) {
        this.formErrors[field] = control.errors;
      }
    }
  }
}

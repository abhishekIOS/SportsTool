import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FuseConfigService } from '../../core/services/config.service';
import { fuseAnimations } from '../../core/animations';
import { AuthService } from '../../st-services/auth/auth.service';
import { Router } from '@angular/router';
import { ReCaptchaComponent } from 'angular2-recaptcha';
@Component({
  selector: 'fuse-st-register',
  templateUrl: './st-register.component.html',
  styleUrls: ['./st-register.component.scss'],
  animations : fuseAnimations
})
export class StRegisterComponent implements OnInit {

    registerForm: FormGroup;
    registerFormErrors: any;
    displayname = '';
    email = '';
    password = '';
    errorMessage = '';
    checkBot = true;
    error: {name: string, message: string} = {name: '', message: ''};
    @ViewChild(ReCaptchaComponent) captcha: ReCaptchaComponent;
    constructor(
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        public authService: AuthService, private router: Router
    )
    {
        this.fuseConfig.setSettings({
            layout: {
                navigation: 'none',
                toolbar   : 'none',
                footer    : 'none'
            }
        });

        this.registerFormErrors = {
            name           : {},
            email          : {},
            password       : {},
            passwordConfirm: {}
        };
    }

    ngOnInit()
    {
        this.registerForm = this.formBuilder.group({
            name           : ['', Validators.required],
            email          : ['', [Validators.required, Validators.email]],
            password       : ['', Validators.required],
            passwordConfirm: ['', [Validators.required, confirmPassword]]
        });

        this.registerForm.valueChanges.subscribe(() => {
            this.onRegisterFormValuesChanged();
        });
    }

    resolved(captchaResponse: string) {
        console.log(`Resolved captcha with response ${captchaResponse}:`);
        this.checkBot = false;
    }

    resetForm() {
        this.captcha.reset();
        this.checkBot = true;
    }

    async onSignUp(): Promise<void>{
        this.clearErrorMessage();
        const result = await this.authService.signUpWithEmail(this.email, this.password, this.displayname);
        if (result === true) {
            this.router.navigate(['/login']);
        }
        else if (result.code) {
            this.errorMessage = result.message;
        }

    }

    clearErrorMessage() {
        this.errorMessage = '';
        this.error = { name: '', message: '' };
    }

    onRegisterFormValuesChanged()
    {
        for ( const field in this.registerFormErrors )
        {
            if ( !this.registerFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.registerFormErrors[field] = {};

            // Get the control
            const control = this.registerForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.registerFormErrors[field] = control.errors;
            }
        }
    }
}

function confirmPassword(control: AbstractControl)
{
    if ( !control.parent || !control )
    {
        return;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if ( !password || !passwordConfirm )
    {
        return;
    }

    if ( passwordConfirm.value === '' )
    {
        return;
    }

    if ( password.value !== passwordConfirm.value )
    {
        return {
            passwordsNotMatch: true
        };
    }
}



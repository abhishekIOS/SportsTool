import { NgModule } from '@angular/core';
import { SharedModule } from '../../core/modules/shared.module';
import { RouterModule } from '@angular/router';

import { StRegisterComponent } from './st-register.component';

const routes = [
    {
        path     : '',
        component: StRegisterComponent
    }
];

@NgModule({
    declarations: [
        StRegisterComponent
    ],
    imports     : [
        SharedModule,
        RouterModule.forChild(routes)
    ]
})

export class RegisterModule
{

}

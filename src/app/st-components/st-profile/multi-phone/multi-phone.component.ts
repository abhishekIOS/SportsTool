import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'fuse-multi-phone',
  templateUrl: './multi-phone.component.html',
  styleUrls: ['./multi-phone.component.scss']
})
export class MultiPhoneComponent implements OnInit {

    @Input() phone;
  constructor() { }

  ngOnInit() {
  }

}

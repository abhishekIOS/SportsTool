import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiEmailComponent } from './multi-email.component';

describe('MultiEmailComponent', () => {
  let component: MultiEmailComponent;
  let fixture: ComponentFixture<MultiEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

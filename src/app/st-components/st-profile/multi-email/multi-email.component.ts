import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'fuse-multi-email',
  templateUrl: './multi-email.component.html',
  styleUrls: ['./multi-email.component.scss']
})
export class MultiEmailComponent implements OnInit {

    @Input() email;
  constructor() { }

  ngOnInit() {
  }

}

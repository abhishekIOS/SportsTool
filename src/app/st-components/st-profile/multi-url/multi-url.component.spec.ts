import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiUrlComponent } from './multi-url.component';

describe('MultiUrlComponent', () => {
  let component: MultiUrlComponent;
  let fixture: ComponentFixture<MultiUrlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiUrlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiUrlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

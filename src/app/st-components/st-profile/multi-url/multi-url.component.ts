import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'fuse-multi-url',
  templateUrl: './multi-url.component.html',
  styleUrls: ['./multi-url.component.scss']
})
export class MultiUrlComponent implements OnInit {

    @Input() url;
  constructor() { }

  ngOnInit() {
  }

}

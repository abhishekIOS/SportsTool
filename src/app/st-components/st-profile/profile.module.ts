import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { SharedModule } from '../../core/modules/shared.module';
import { MatDialogModule } from '@angular/material';

const routes = [
  {
    path: '',
    component: ProfileComponent
  }
];

@NgModule({
  imports: [
    CommonModule, 
    SharedModule,
    MatDialogModule,
    RouterModule.forChild(routes)],
    declarations: [ProfileComponent]
})
export class ProfileModule {}

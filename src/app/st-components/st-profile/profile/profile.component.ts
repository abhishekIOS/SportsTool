import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { UserService } from '../shared/user.service';
import { Profile} from '../../../st-models/profile.model';
import { AngularFirestore } from 'angularfire2/firestore';
import {UploadService} from '../../../st-services/upload/upload.service';
import { CountryService } from '../../../st-services/country/country.service';
import {Upload} from '../../../st-models/upload.model';
import { fuseAnimations } from '../../../core/animations';
import * as firebase from 'firebase';
// import { MdToast } from 'md-toast';
import { Observable } from 'rxjs/Observable';
import 'moment';
declare let moment: any;
import { MatDialog, MatDialogRef } from '@angular/material';
import { WebCameraComponent } from '../web-camera/web-camera.component';
import { Router } from '@angular/router';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import { MatSnackBar } from '@angular/material';
import * as _ from 'lodash';

@Component({
  selector: 'fuse-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [UserService, CountryService],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations,
})
export class ProfileComponent implements OnInit {
  form: FormGroup;
  formErrors: any;
  item: Observable<any>;
  sports$: Observable<any>;
  users$: Observable<any>;
  roles$: Observable<any>;
  socialnetworks: any = [];
  countries$: any = [];
  birthfilteredCountries: any = [];
  filteredCountries: any = [];
  profile: any;
  profiles: any = [];
  logoURL: any = null;
    docURL: any = null;
    selectedFiles: any;
    selectedDocFiles: any;

    FileUpload: Upload;
    dialogRef: any;

    currentFileUpload: Upload;
    disableBtn = false;


  constructor( private formBuilder: FormBuilder, public dialog: MatDialog,
               private afs: AngularFirestore, private userService: UserService,
               private uploadService: UploadService,
               @Inject(SESSION_STORAGE) private storage: StorageService,
               private router: Router,
               public snackBar: MatSnackBar,
               private countryservice: CountryService) {

      this.selectedFiles = null;
      this.selectedDocFiles = null;
    // Reactive form errors
    this.formErrors = {
        firstName: {},
        lastName: {},
        nickName: {},
        gender: {},
        birthday: {},
        cityOfBirth: {},
        countryOfBirth: {},
        sports: {},
        roles: {},
        emails: {},
        phones: {},
        urls: {},
        socialaccounts: {},
        address: {},
        address2: {},
        city: {},
        state: {},
        postalCode: {},
        country: {},
        adaptive_status: {},
        adaptive_items: {},
        adaptive_note: {},
    };

      // Reactive Form
      this.form = this.formBuilder.group({
          firstName: ['', Validators.required],
          lastName: ['', Validators.required],
          nickName: [''],
          gender: ['', Validators.required],
          birthday: ['', Validators.required],
          cityOfBirth: [''],
          countryOfBirth: [''],
          sports: ['', Validators.required],
          roles: ['', Validators.required],
          urls: this.formBuilder.array([ this.createItem('url')]),
          emails: this.formBuilder.array([ this.createItem('email')]),
          phones: this.formBuilder.array([ this.createItem('phone')]),
          socialaccounts: this.formBuilder.array([ this.createItem('social')]),
          address: ['', Validators.required],
          address2: [''],
          city: ['', Validators.required],
          state: ['', Validators.required],
          postalCode: ['', [Validators.required, Validators.maxLength(5)]],
          country: ['', Validators.required],
          adaptive_status: [''],
          adaptive_items: this.formBuilder.array([ this.createItem('adapt')]),
          adaptive_note: ['']
      });

      this.countryservice.getCountries().subscribe(
          resultArray => this.countries$ = resultArray,
          error => console.log('Error :: ' + error)
      );
    // this will subscribe and console.log all the users in firestore upon
    this.users$ = this.afs.collection<any>('users').valueChanges();
    this.sports$ = this.afs.collection<any>('sports').valueChanges();
      this.roles$ = this.afs.collection<any>('roles').valueChanges();
      // this.socialnetworks$ = this.afs.collection<any>('socialnetwork').valueChanges();
      const query = firebase.firestore().collection('socialnetwork');
      query.onSnapshot((snapshot) => {
          this.socialnetworks = [];
          snapshot.forEach((doc) => {
              this.socialnetworks.push(doc.data());
          });
      }, (error) => {
          console.log(error);
      });

      const userid = this.storage.get('uid');
      if (userid) {
          firebase.firestore().collection('profiles').doc(userid).get().then((doc) => {
              if (doc.exists) {
                        this.profile = doc.data();
                        if (this.profile.emails.length > 0) {
                            for (let i = 0; i < this.profile.emails.length; i++) {
                                this.emails.push(this.formBuilder.group({
                                    role: [this.profile.emails[i].role],
                                    email: [this.profile.emails[i].email]
                                }));
                            }
                            this.emails.removeAt(0);
                        }

                      if (this.profile.phones.length > 0) {
                          for (let i = 0; i < this.profile.phones.length; i++) {
                              this.phones.push(this.formBuilder.group({
                                  role: [this.profile.phones[i].role],
                                  phone: [this.profile.phones[i].phone]
                              }));
                          }
                          this.phones.removeAt(0);

                      }

                      if (this.profile.urls.length > 0) {
                          for (let i = 0; i < this.profile.urls.length; i++) {
                              this.urls.push(this.formBuilder.group({
                                  link: [this.profile.urls[i].link]
                              }));
                          }
                          this.urls.removeAt(0);

                      }

                      if (this.profile.socialaccounts.length > 0) {
                          for (let i = 0; i < this.profile.socialaccounts.length; i++) {
                              this.socialaccounts.push(this.formBuilder.group({
                                  social: [this.profile.socialaccounts[i].social],
                                  id: [this.profile.socialaccounts[i].id]
                              }));
                          }
                          this.socialaccounts.removeAt(0);

                      }
                  if (this.profile.adaptive.items.length > 0) {
                      for (let i = 0; i < this.profile.adaptive.items.length; i++) {
                          this.adaptive_items.push(this.formBuilder.group({
                              name: [this.profile.adaptive.items[i].name],
                              status: [this.profile.adaptive.items[i].status],
                              value: [this.profile.adaptive.items[i].value]
                          }));
                      }
                      this.adaptive_items.removeAt(0);
                  } else {
                      firebase.firestore().collection('AdaptItems').onSnapshot((snapshot) => {
                          snapshot.forEach((doca) => {
                              this.adaptive_items.push(this.formBuilder.group({
                                  name: [doca.data().name],
                                  status: [false],
                                  value: [1]
                              }));
                          });
                          this.adaptive_items.removeAt(0);
                      }, (error) => {
                          console.log(error);
                      });
                  }

                    if (this.profile.logo) {
                        this.logoURL = this.profile.logo.url;
                    }
                      if (this.profile.picture) {
                          this.docURL = this.profile.picture.url;
                      }
              } else {
                  // doc.data() will be undefined in this case
                  console.log('No such document!');
              }
          }).catch(function(error) {
              console.log('Error getting document:', error);
          });
      } else {
          this.router.navigate(['/']);
      }
  }

  ngOnInit() {

    this.form.valueChanges.subscribe(() => {
      this.onFormValuesChanged();
      if (this.form.value.birthday) {
        const bday = new Date(moment(this.form.value.birthday));
        const age = this.calculateAge(bday.getMonth(), bday.getDate(), bday.getFullYear());
        this.profile.age = age;
      }
        if (this.form.value.country) {
            this.filteredCountries = this.filterCountries(this.form.value.country);
        }
        if (this.form.value.countryOfBirth) {
            this.birthfilteredCountries = this.filterCountries(this.form.value.countryOfBirth);
        }
    });
  }

    createItem(item): FormGroup {
        if (item === 'social') {
            return this.formBuilder.group({
                social: [''],
                id: ['']
            });
        }

        else if (item === 'url') {
            return this.formBuilder.group({
                link: ['']
            });
        }
        else if (item === 'phone') {
            return this.formBuilder.group({
                role: [''],
                phone: ['', Validators.required]
            });
        }
        else if (item === 'email') {
            return this.formBuilder.group({
                role: [''],
                email: ['', Validators.required]
            });
        }
        else if (item === 'adapt') {
            return this.formBuilder.group({
                name: [''],
                status: [''],
                value: ['']
            });
        }

    }

    get socialaccounts(): FormArray {
        return this.form.get('socialaccounts') as FormArray;
    }

    get urls(): FormArray {
        return this.form.get('urls') as FormArray;
    }

    get phones(): FormArray {
        return this.form.get('phones') as FormArray;
    }

    get emails(): FormArray {
        return this.form.get('emails') as FormArray;
    }

    get adaptive_items(): FormArray {
        return this.form.get('adaptive_items') as FormArray;
    }

    deleteItem(item, index): void {
        if (index === 0) {
            return;
        }
        if (item === 'url') {
            this.urls.removeAt(index);
        }
        else if (item === 'social') {
            this.socialaccounts.removeAt(index);
        }
        else if (item === 'phone') {
            this.phones.removeAt(index);
        }
        else if (item === 'email') {
            this.emails.removeAt(index);
        }
    }

    addItem(item): void {
        if (item === 'url') {
            this.urls.push(this.createItem(item));
        }
        else if (item === 'social') {
            this.socialaccounts.push(this.createItem(item));
        }
        else if (item === 'phone') {
            this.phones.push(this.createItem(item));
        }
        else if (item === 'email') {
            this.emails.push(this.createItem(item));
        }
    }

  calculateAge(birthMonth, birthDay, birthYear): number{
      const currentDate = new Date();
      const currentYear = currentDate.getFullYear();
      const currentMonth = currentDate.getMonth();
      const currentDay = currentDate.getDate();
      let calculatedAge = currentYear - birthYear;
      if (currentMonth < birthMonth ) {
          calculatedAge--;
      }
      if (birthMonth  === currentMonth && currentDay < birthDay) {
          calculatedAge--;
      }
      return calculatedAge;
  }

    filterCountries(name: string) {
        return this.countries$.filter(country =>
            country.Name.toLowerCase().indexOf(name.toLowerCase()) === 0);
    }
  async onSubmit(): Promise<void> {
      if (this.form.invalid) {
          return;
      }
      this.disableBtn = true;
      const editprofile = _.cloneDeep(this.profile);
      editprofile.emails = this.form.value.emails;
      editprofile.phones = this.form.value.phones;
      editprofile.socialaccounts = this.form.value.socialaccounts;
      editprofile.urls = this.form.value.urls;
      editprofile.adaptive.items = this.form.value.adaptive_items;
      editprofile.adaptive = Object.assign({}, editprofile.adaptive);
      const result = await this.userService.insertItem(editprofile);
      if (result) {
          try {
              if (this.selectedFiles) {
                  this.FileUpload = new Upload(this.selectedFiles);
                  this.FileUpload.key = editprofile.uid;
                  await this.uploadService.pushFileToStorage(this.FileUpload, 'profiles', 'logo');
              }
              if (this.selectedDocFiles) {
                  let docfile = new Upload(this.selectedDocFiles);
                  docfile.key = editprofile.uid;
                  await this.uploadService.pushFileToStorage(docfile, 'profiles', 'picture');
              }
          } catch (error) {
              await console.log(error);
          }
          this.disableBtn = false;
          this.snackBar.open('update success!', 'Dance', {
              duration: 2000
          });
      } else {
          this.disableBtn = false;
          this.snackBar.open('update failed!', 'Dance', {
              duration: 2000
          });
      }




  }

    async selectlogo(event): Promise<void> {
        this.selectedFiles = event.target.files.item(0);
        this.logoURL = await this.getBase64(event.target.files.item(0));
    }

    async selectDoc(event): Promise<void> {
        this.selectedDocFiles = event.target.files.item(0);
        this.docURL = await this.getBase64(event.target.files.item(0));
    }

    async getBase64(file): Promise<any> {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }

    reset() {
      this.profile = new Profile();
    }

    onFormValuesChanged()
    {
        for ( const field in this.formErrors )
        {
            if ( !this.formErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.formErrors[field] = control.errors;
            }
        }
    }

    async cameracapture(contact): Promise<void>
    {
        this.dialogRef = this.dialog.open(WebCameraComponent, {
            panelClass: 'fuse-web-camera',
            data      : {
                profile: this.profile,
             },
            height: '600px',
            width: '300px',
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }

                this.logoURL = response[0];
                this.selectedFiles = response[1];
            });
    }

    doccameracapture(contact)
    {
        this.dialogRef = this.dialog.open(WebCameraComponent, {
            panelClass: 'fuse-web-camera',
            data      : {
                profile: this.profile,
            },
            height: '600px',
            width: '300px',
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }

                this.docURL = response[0];
                this.selectedDocFiles = response[1];
            });
    }
}

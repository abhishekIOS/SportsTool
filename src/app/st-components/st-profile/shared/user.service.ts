import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import { Profile} from '../../../st-models/profile.model';
import 'moment';
declare let moment: any;
import {Upload} from '../../../st-models/upload.model';
import {UploadService} from '../../../st-services/upload/upload.service';

@Injectable()
export class UserService {
    all_profiles: any[] = [];
    profile: any;
    prodcollection: AngularFirestoreCollection<any>;
    ref: any;
    FileUpload: Upload;

    constructor( private afs: AngularFirestore, private uploadService: UploadService) {
        this.ref = firebase.firestore().collection('profiles');
    }

    async getAllData(): Promise<any[]> {
        this.all_profiles = [];
        return this.ref.get().then((resp) => {
            resp.forEach((doc) => {
                this.all_profiles.push(doc.data());
            });
            return this.all_profiles;
        });
    }

    getItem(key: string): any {
        return this.ref.doc(key).get()
            .then(function(doc) {
                return doc.data();
            });
    }

    async insertItem(profile: any): Promise<any>
    {
        profile.date_update = new Date();

        for (const key of Object.keys(profile)) {
               if ( profile[key] === undefined) {
                   profile[key] = null;
            }
        }

        try {
            const result = this.ref.doc(profile.uid).update(Object.assign({}, profile));
            await console.log('Document written Success!: ');
            return true;
        } catch (error) {
            await console.log(error);
            return false;
        }
    }

    async addnewuser(profile: any, logo: any, doc: any): Promise<any>
    {
        profile.uid =
            moment().format('YYMMMDDTHHmmss') + '-' + Math.random().toString(36).slice(2);
        profile.date_create = new Date();
        profile.date_update = new Date();
        for (const key of Object.keys(profile)) {
            if ( profile[key] === undefined) {
                profile[key] = null;
            }
        }

        try {
            await this.ref.doc(profile.uid).set(Object.assign({}, profile));
            if (logo) {
                this.FileUpload = new Upload(logo);
                this.FileUpload.key = profile.uid;
                await this.uploadService.pushFileToStorage(this.FileUpload, 'profiles', 'logo');
            }
            if (doc) {
                let docUpload = new Upload(doc);
                docUpload.key = profile.uid;
                await this.uploadService.pushFileToStorage(docUpload, 'profiles', 'picture');
            }
            await console.log('Create New User Success!: ');
            return true;
        } catch (error) {
            await console.log(error);
            return false;
        }
    }

    deleteUser(profile) {
        this.prodcollection.doc(profile.id).delete().then(() => {
            console.log('deleted');
        });
    }
}

import { Component, OnInit, Inject } from '@angular/core';
import { WebCamComponent } from 'ack-angular-webcam';
import {UploadService} from '../../../st-services/upload/upload.service';
import {Upload} from '../../../st-models/upload.model';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
@Component({
  selector: 'fuse-web-camera',
  templateUrl: './web-camera.component.html',
  styleUrls: ['./web-camera.component.scss']
})
export class WebCameraComponent implements OnInit {

    imagepath: any;
    profile: any;
    fileUpload: Upload;
    webcam: WebCamComponent; // will be populated by <ack-webcam [(ref)]="webcam">
    base64;
    cameraState = true;
    options = {
        audio: false,
        video: true,
        width: 250,
        height: 450
    };
  constructor(public dialogRef: MatDialogRef<WebCameraComponent>, private uploadService: UploadService,
              @Inject(MAT_DIALOG_DATA) private data: any) {
    this.profile = data.profile;
  }

  ngOnInit() {

  }

    uploadimage($event){
        // $event.preventDefault();
      if (this.cameraState === true) {
          this.webcam.getBase64()
              .then( base => {this.base64 = base;
                  const blob = this.dataURItoBlob(base);
                  const image = new File([blob], 'image.png');
                  // this.fileUpload = new Upload(image);
                  // this.fileUpload.key = this.profile.uid;
                  // this.imagepath = this.uploadService.pushFileToStorage(this.fileUpload, 'profiles', 'logo');
                  this.dialogRef.close([this.base64, image]);
              })
              .catch( e => console.error(e) );
      } else {
          this.dialogRef.close();
      }

    }

    dataURItoBlob(dataURI) {
        const byteString = atob(dataURI.toString().split(',')[1]);

        // var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        const ab = new ArrayBuffer(byteString.length);
        const ia = new Uint8Array(ab);
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        const blob = new Blob([ab], {type: 'image/png'}); // or mimeString if you want
        return blob;
    }
    onCamError(err){
        this.cameraState = false;
    }

    onCamSuccess(event){
        this.cameraState = true;
    }
}

import { NgModule } from '@angular/core';
import { SharedModule } from '../../core/modules/shared.module';
import { RouterModule } from '@angular/router';
import { StForgotPasswdComponent } from './st-forgot-passwd.component';

const routes = [
    {
        path     : '',
        component: StForgotPasswdComponent
    }
];

@NgModule({
    declarations: [
        StForgotPasswdComponent
    ],
    imports     : [
        SharedModule,
        RouterModule.forChild(routes)
    ]
})

export class ForgotPasswordModule
{

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StForgotPasswdComponent } from './st-forgot-passwd.component';

describe('StForgotPasswdComponent', () => {
  let component: StForgotPasswdComponent;
  let fixture: ComponentFixture<StForgotPasswdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StForgotPasswdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StForgotPasswdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StColorPickerComponent } from './st-color-picker.component';

describe('StColorPickerComponent', () => {
  let component: StColorPickerComponent;
  let fixture: ComponentFixture<StColorPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StColorPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StColorPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

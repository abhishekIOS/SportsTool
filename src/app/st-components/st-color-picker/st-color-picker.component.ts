import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'fuse-st-color-picker',
  templateUrl: './st-color-picker.component.html',
  styleUrls: ['./st-color-picker.component.scss']
})
export class StColorPickerComponent implements OnInit {

    @Input() color: any;
    @Output() create = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }

    onSettingsColor(event) {
      this.create.emit(this.color);
    }
}

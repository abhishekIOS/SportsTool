import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import * as firebase from 'firebase';

@Component({
  selector: 'fuse-team-detail',
  templateUrl: './team-detail.component.html',
  styleUrls: ['./team-detail.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class TeamDetailComponent implements OnInit {

    team: any;
    owner: any;
    club: any;
    constructor( public dialogRef: MatDialogRef<TeamDetailComponent>,
                 @Inject(MAT_DIALOG_DATA) private data: any) {
        this.team = data.team;
        if (this.team.owner_id) {
            firebase.firestore().collection('profiles').doc(this.team.owner_id).get().then((doc) => {
                this.owner = doc.data();
            });

        }

        if (this.team.club_id) {
            firebase.firestore().collection('clubs').doc(this.team.club_id).get().then((doc) => {
                this.club = doc.data();
            });

        }
    }

    ngOnInit() {
    }

}

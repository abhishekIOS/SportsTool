import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { TeamFormComponent } from '../team-form/team-form.component';
import { AngularFirestore } from 'angularfire2/firestore';
import { fuseAnimations } from '../../../core/animations';
import * as firebase from 'firebase';
import { Observable } from 'rxjs/Observable';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import {UploadService} from '../../../st-services/upload/upload.service';
import {Upload} from '../../../st-models/upload.model';
import 'moment';
declare let moment: any;
import { CountryService } from '../../../st-services/country/country.service';

@Component({
  selector: 'fuse-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss'],
    providers: [ CountryService],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
})
export class TeamComponent implements OnInit {
    item: Observable<any>;
    teams: any = [];
    searchInput: FormControl;
    dialogRef: any;
    currentUserID: string;
    FileUpload: Upload;
    countries: any = [];
    constructor( private afs: AngularFirestore,
                 public dialog: MatDialog,
                 @Inject(SESSION_STORAGE) private storage: StorageService,
                 private uploadService: UploadService,
                 private countryservice: CountryService)
    {
        this.searchInput = new FormControl('');
        this.currentUserID = this.storage.get('uid');
        this.countryservice.getCountries().subscribe(
            resultArray => this.countries = resultArray,
            error => console.log('Error :: ' + error)
        );
    }

  ngOnInit() {

  }

    newTeam()
    {
        this.dialogRef = this.dialog.open(TeamFormComponent, {
            panelClass: 'contact-form-dialog',
            data      : {
                countries: this.countries,
                action: 'new'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: any) => {
                if ( !response || !this.currentUserID )
                {
                    return;
                }

                const formData: any = response[0];
                const editteam: any = response[1];
                const logoimage: any = response[2];
                const description: any = response[3];
                this.addNewTeam(formData.value, editteam, logoimage, description);
            });

    }

    async addNewTeam(team, editteam, logo, description): Promise<void> {
        // set color
        team.homecolor = Object.assign({}, editteam.homecolor);
        team.awaycolor = Object.assign({}, editteam.awaycolor);
        // set unique ID
        const teamid =
            moment().format('YYMMMDDTHHmmss') +
            '-' +
            Math.random()
                .toString(36)
                .slice(2);
        team.date_create = new Date();
        team.date_update = new Date();
        team.id = teamid;
        team.owner_id = this.currentUserID;
        await firebase.firestore().collection('teams').doc(teamid).set(Object.assign({}, team));
        if (logo) {
            this.FileUpload = new Upload(logo);
            this.FileUpload.key = team.id;
            team.logo = await this.uploadService.pushFileToStorage(this.FileUpload, 'teams', 'logo');

        }

        if (description) {
            const descripFile = new Upload(description);
            descripFile.key = team.id;
            team.description = await this.uploadService.pushFileToStorage(descripFile, 'teams', 'description');

        }
    }

}

import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { UploadService } from '../../../st-services/upload/upload.service';
import { Team } from '../../../st-models/team.model';
import { Profile } from '../../../st-models/profile.model';
import { AngularFirestore } from 'angularfire2/firestore';
import { Upload} from '../../../st-models/upload.model';
import { fuseAnimations } from '../../../core/animations';
import { Observable } from 'rxjs/Observable';
import 'moment';
declare let moment: any;
import * as _ from 'lodash';
import { MatDialog, MatDialogRef } from '@angular/material';
import { WebCameraComponent } from '../../st-profile/web-camera/web-camera.component';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import { MatSnackBar } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../core/components/confirm-dialog/confirm-dialog.component';
import { Router } from '@angular/router';
import * as firebase from 'firebase';

@Component({
  selector: 'fuse-new-team',
  templateUrl: './new-team.component.html',
  styleUrls: ['./new-team.component.scss'],
    providers: [UploadService],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
})
export class NewTeamComponent implements OnInit {

    form: FormGroup;
    formErrors: any;
    sports$: Observable<any>;
    clubs$: Observable<any>;
    socialnetworks: any = [];
    filteredCountries: any = [];
    adaptItems: any = [];
    adultAges: any = [];
    youthAges: any = [];
    newteam: Team;
    selectedClub: any;
    logoURL: any = null;
    pictureURL: any = null;
    dialogRef: any;
    selectedFiles: any;
    selectedDocFiles: any;
    ref: any;
    FileUpload: Upload;
    disableBtn = false;
    currentUser: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    constructor( private formBuilder: FormBuilder, public dialog: MatDialog,
                 private afs: AngularFirestore, private uploadService: UploadService,
                 @Inject(SESSION_STORAGE) private storage: StorageService,
                 public snackBar: MatSnackBar,
                 private router: Router) {
        this.newteam = new Team();

        // Reactive form errors
        this.formErrors = {
            name: {},
            gender: {},
            size: {},
            age: {},
            club_id: {},
            sports: {},
            adaptive_status: {},
            adaptive_items: {},
            adaptive_note: {},
            description: {}
        };

        // Reactive Form
        this.form = this.formBuilder.group({
            name: ['', Validators.required],
            gender: ['', Validators.required],
            age :  this.formBuilder.group({
                level: [this.newteam.age.level],
                old: [this.newteam.age.old]
            }),
            size: ['', Validators.required],
            club_id: ['', Validators.required],
            sports: ['', Validators.required],
            adaptive_status: [''],
            adaptive_items: this.formBuilder.array([ this.createItem('adapt')]),
            adaptive_note: [''],
            description: ['']
        });

        this.ref = firebase.firestore().collection('teams');
        this.selectedFiles = null;
        this.selectedDocFiles = null;
        this.sports$ = this.afs.collection<any>('sports').valueChanges();
        this.clubs$ = this.afs.collection<any>('clubs').valueChanges();
        const query = firebase.firestore().collection('socialnetwork');
        query.onSnapshot((snapshot) => {
            this.socialnetworks = [];
            snapshot.forEach((doc) => {
                this.socialnetworks.push(doc.data());
            });
        }, (error) => {
            console.log(error);
        });

        firebase.firestore().collection('AdaptItems').onSnapshot((snapshot) => {
            this.adaptItems = [];
            snapshot.forEach((doc) => {
                this.adaptive_items.push(this.formBuilder.group({
                    name: [doc.data().name],
                    status: [false],
                    value: [1]
                }));
            });
            this.adaptive_items.removeAt(0);
        }, (error) => {
            console.log(error);
        });

        firebase.firestore().collection('age').doc('age').get()
            .then((doc) => {
                this.adultAges = [];
                this.youthAges = [];
                if (doc.exists) {
                    this.adultAges = doc.data().adult;
                    this.youthAges = doc.data().youth;
                } else {
                    console.log('No age document!');
                }
            }).catch((error) => {
            console.log('Error getting age:', error);
        });
    }

    async ngOnInit(): Promise<void> {

        if (this.storage.get('uid')) {
            firebase.firestore().collection('profiles').doc(this.storage.get('uid')).get().then((doc) => {
                this.currentUser = doc.data();
            });
        } else {
            this.confirmUser();
        }

        this.form.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
            if (this.form.value.country) {
                this.filteredCountries = this.filterCountries(this.form.value.country);
            }
        });

    }

    onSelectClub(club) {
        if (club) {
            this.newteam.club_id = club.id;
            this.newteam.emails = club.emails;
            this.newteam.phones = club.phones;
        }
    }

    createItem(item): FormGroup {
        if (item === 'adapt') {
            return this.formBuilder.group({
                name: [''],
                status: [''],
                value: ['']
            });
        }
    }

    get adaptive_items(): FormArray {
        return this.form.get('adaptive_items') as FormArray;
    }

    confirmUser() {
        this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
            disableClose: true
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Sorry, this action is needed login on site';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if ( result ) {
                this.router.navigate(['/login']);
            } else {
                this.router.navigate(['/home']);
            }
            this.confirmDialogRef = null;
        });
    }

    filterCountries(name: string) {
        // return this.countries.filter(country =>
        //     country.Name.toLowerCase().indexOf(name.toLowerCase()) === 0);
    }
    async onSubmit(): Promise<void> {
        if (this.form.invalid) {
            return;
        }
        this.disableBtn = true;
        const teamid =
            moment().format('YYMMMDDTHHmmss') +
            '-' +
            Math.random()
                .toString(36)
                .slice(2);
        this.newteam.id = teamid;
        // this.newteam.homecolor = Object.assign({}, editteam.homecolor);
        this.newteam.owner_id = this.currentUser.uid;
        this.newteam.date_create = new Date();
        this.newteam.date_update = new Date();
        this.newteam.socialaccounts = this.currentUser.socialaccounts;
        this.newteam.urls = this.currentUser.urls;
        this.newteam.age = Object.assign({}, this.newteam.age);
        this.newteam.awaycolor = Object.assign({}, this.newteam.awaycolor);
        this.newteam.homecolor = Object.assign({}, this.newteam.homecolor);
        this.newteam.adaptive.items = this.form.value.adaptive_items;
        this.newteam.adaptive = Object.assign({}, this.newteam.adaptive);

        const result = await this.addnewteam(this.newteam, this.selectedFiles, this.selectedDocFiles);
        if (result) {
            this.disableBtn = false;
            this.snackBar.open('Add new team success!', 'Dance', {
                duration: 3000
            });
        } else {
            this.disableBtn = false;
            this.snackBar.open('Add new team failed!', 'Dance', {
                duration: 3000
            });
        }

    }

    async addnewteam(team: any, logo: any, picture: any): Promise<any>
    {
        for (const key of Object.keys(team)) {
            if ( team[key] === undefined) {
                team[key] = null;
            }
        }

        try {
            await this.ref.doc(team.id).set(Object.assign({}, team));
            if (logo) {
                this.FileUpload = new Upload(logo);
                this.FileUpload.key = team.id;
                await this.uploadService.pushFileToStorage(this.FileUpload, 'teams', 'logo');
            }
            if (picture) {
                let uplodadpicture = new Upload(picture);
                uplodadpicture.key = team.id;
                await this.uploadService.pushFileToStorage(uplodadpicture, 'teams', 'picture');
            }
            await console.log('Create New team Success!: ');
            return true;
        } catch (error) {
            await console.log(error);
            return false;
        }
    }

    async selectlogo(event): Promise<void> {
        this.selectedFiles = event.target.files.item(0);
        this.logoURL = await this.getBase64(event.target.files.item(0));
    }

    async selectDoc(event): Promise<void> {
        this.selectedDocFiles = event.target.files.item(0);
        this.pictureURL = await this.getBase64(event.target.files.item(0));
    }

    async getBase64(file): Promise<any> {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }

    reset() {
        this.newteam = new Team();
    }

    prevent(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
        }
    }

    onFormValuesChanged()
    {
        for ( const field in this.formErrors )
        {
            if ( !this.formErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.formErrors[field] = {};

            // Get the control
            const control = this.form.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.formErrors[field] = control.errors;
            }
        }
    }

    cameracapture()
    {
        this.dialogRef = this.dialog.open(WebCameraComponent, {
            panelClass: 'fuse-web-camera',
            data      : {
                profile: this.newteam,
            },
            height: '600px',
            width: '300px',
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }

                this.logoURL = response[0];
                this.selectedFiles = response[1];
            });
    }

    doccameracapture()
    {
        this.dialogRef = this.dialog.open(WebCameraComponent, {
            panelClass: 'fuse-web-camera',
            data      : {
                profile: this.newteam,
            },
            height: '600px',
            width: '300px',
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }

                this.pictureURL = response[0];
                this.selectedDocFiles = response[1];
            });
    }
}


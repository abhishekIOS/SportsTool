import { Component, OnInit, ViewEncapsulation, Input, ViewChild } from '@angular/core';
import { fuseAnimations } from '../../../core/animations';
import * as firebase from 'firebase';
import { TeamFormMenuComponent } from '../team-form-menu/team-form-menu.component';
import { LogoComponent } from '../../ag-grid/logo/logo.component';

@Component({
  selector: 'fuse-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
})
export class TeamListComponent implements OnInit {

    @Input() currentUserID: string;
    teams: any[] = [];
    filter: any = {
        name: '',
        shirt_color: '',
        gender: ''
    };
    columnDefs;
    context;
    rowData: any[];
    frameworkComponents = {
        TeamFormMenuComponent: TeamFormMenuComponent,
        LogoComponent: LogoComponent
    };
    constructor() {
        this.columnDefs = [
            {headerName: 'Logo',
                field: 'logo',
                cellRenderer: 'LogoComponent',
                suppressSorting: false,
                suppressMenu: true,
                width: 80
            },
            {headerName: 'name', field: 'name'},
            {headerName: 'gender', field: 'gender' },
            {headerName: 'size', field: 'size' },
            {headerName: 'sports', field: 'sports' },
            {
                headerName: '',
                // field: 'make',
                editable: false,
                cellRenderer: 'TeamFormMenuComponent',
                // colId: 'make',
                width: 70
            },

        ];
        this.context = { componentParent: this };

    }

    async ngOnInit() {
        await this.getAllTeams();
    }

    async getAllTeams() {
        const query = firebase.firestore().collection('teams');
        query.onSnapshot((snapshot) => {
            this.teams = [];
            snapshot.forEach((doc) => {
                this.teams.push(doc.data());
            });
            // this.dataSource = new MatTableDataSource(this.teams);
            // this.dataSource.paginator = this.paginator;
            // this.dataSource.sort = this.sort;
            this.rowData = this.teams;
        }, (error) => {
            console.log(error);
        });
    }
}

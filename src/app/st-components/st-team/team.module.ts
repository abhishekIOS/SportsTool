import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TeamComponent } from './team/team.component';
import { SharedModule } from '../../core/modules/shared.module';
import { TeamFormComponent } from './team-form/team-form.component';
import { TeamListComponent } from './team-list/team-list.component';
import { StColorPickerComponent } from '../st-color-picker/st-color-picker.component';
import { TeamDetailComponent } from './team-detail/team-detail.component';
import { NewTeamComponent } from './new-team/new-team.component';

const routes = [
  {
    path: '',
    component: TeamComponent
  },
    {
        path     : 'newteam',
        component: NewTeamComponent
    },
    {
        path     : 'allteams',
        component: TeamComponent
    }
];

@NgModule({
  imports: [
    CommonModule, 
    SharedModule,
    RouterModule.forChild(routes)],
  declarations: [TeamComponent, TeamFormComponent, TeamListComponent, StColorPickerComponent, TeamDetailComponent, NewTeamComponent],
    entryComponents: [TeamFormComponent, TeamDetailComponent]
})
export class TeamModule {}

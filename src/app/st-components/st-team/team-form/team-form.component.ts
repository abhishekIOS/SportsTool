import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Team } from '../../../st-models/team.model';
import { Club } from '../../../st-models/club.model';
import {FormControl} from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import { WebCameraComponent } from '../../st-profile/web-camera/web-camera.component';

@Component({
  selector: 'fuse-team-form',
  templateUrl: './team-form.component.html',
  styleUrls: ['./team-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class TeamFormComponent implements OnInit {

    event: CalendarEvent;
    dialogTitle: string;
    teamForm: FormGroup;
    formErrors: any;
    selectedClub: any;
    adultAges: any = [];
    youthAges: any = [];
    action: string;
    team: Team;
    countries: any = [];
    filteredCountries: any = [];
    logoURL: any;
    pictureURL; any;
    uploadDescripFile: any;
    sports$: Observable<any>;
    clubs$: Observable<any>;
    adults$: any = [];
    logodialogRef: any;
    selectedFiles: any;
    selectedDocFiles: any;
    constructor( public dialogRef: MatDialogRef<TeamFormComponent>,
                 public dialog: MatDialog,
                 @Inject(MAT_DIALOG_DATA) private data: any,
                 private formBuilder: FormBuilder,
                 private afs: AngularFirestore ) {

        this.selectedClub = new Club();
        // Reactive form errors
        this.formErrors = {
            name: {},
            gender: {},
            size: {},
            age: {},
            club_id: {},
            sports: {},
            adaptive_status: {},
            adaptive_items: {},
            adaptive_note: {},
            description: {}
        };

        // Reactive Form
        this.teamForm = this.formBuilder.group({
            name: ['', Validators.required],
            gender: ['', Validators.required],
            age :  this.formBuilder.group({
                level: [],
                old: []
            }),
            size: ['', Validators.required],
            club_id: ['', Validators.required],
            sports: ['', Validators.required],
            adaptive_status: [''],
            adaptive_items: this.formBuilder.array([ this.createItem('adapt')]),
            adaptive_note: [''],
            description: ['']
        });

        this.selectedFiles = null;
        this.selectedDocFiles = null;
        this.uploadDescripFile = null;
        this.logoURL = null;
        this.pictureURL = null;
        this.action = data.action;
        this.sports$ = this.afs.collection<any>('sports').valueChanges();
        this.clubs$ = this.afs.collection<any>('clubs').valueChanges();
        firebase.firestore().collection('age').where('value', '>=', 'U15')
            .onSnapshot((snapshot) => {
                this.adults$ = [];
                snapshot.forEach((doc) => {
                    this.adults$.push(doc.data());
                });
            });

        if ( this.action === 'edit' )
        {
            this.dialogTitle = 'Edit Team';
            this.team = data.team;
            if (this.team.logo) {
                this.logoURL = this.team.logo.url;
            }
            if (this.team.picture) {
                this.pictureURL = this.team.picture.url;
            }
            if (this.team.club_id) {
                firebase.firestore().collection('clubs').doc(this.team.club_id).get().then((doc) => {
                    this.selectedClub = doc.data();
                });
            }
            if (this.team.adaptive.items.length > 0) {
                for (let i = 0; i < this.team.adaptive.items.length; i++) {
                    this.adaptive_items.push(this.formBuilder.group({
                        name: [this.team.adaptive.items[i].name],
                        status: [this.team.adaptive.items[i].status],
                        value: [this.team.adaptive.items[i].value]
                    }));
                }
                this.adaptive_items.removeAt(0);

            }
            this.countries = data.countries;
            this.filteredCountries = this.countries;
        }

        firebase.firestore().collection('age').doc('age').get()
            .then((doc) => {
                this.adultAges = [];
                this.youthAges = [];
                if (doc.exists) {
                    this.adultAges = doc.data().adult;
                    this.youthAges = doc.data().youth;
                } else {
                    console.log('No age document!');
                }
            }).catch((error) => {
            console.log('Error getting age:', error);
        });

    }

    ngOnInit() {

    }

    onSelectClub(club) {
        if (club) {
            this.team.club_id = club.id;
            this.team.emails = club.emails;
            this.team.phones = club.phones;
        }
    }

    async selectlogo(event): Promise<void> {
        this.selectedFiles = event.target.files.item(0);
        this.logoURL = await this.getBase64(event.target.files.item(0));
    }

    async selectDoc(event): Promise<void> {
        this.selectedDocFiles = event.target.files.item(0);
        this.pictureURL = await this.getBase64(event.target.files.item(0));
    }

    async getBase64(file): Promise<any> {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }

    createItem(item): FormGroup {
        if (item === 'adapt') {
            return this.formBuilder.group({
                name: [''],
                status: [''],
                value: ['']
            });
        }
    }

    get adaptive_items(): FormArray {
        return this.teamForm.get('adaptive_items') as FormArray;
    }

    applyFilter(value) {
        if (value) {
            this.filteredCountries = this.filterCountries(value);
        }
    }

    filterCountries(name: string) {
        return this.countries.filter(country =>
            country.Name.toLowerCase().indexOf(name.toLowerCase()) === 0);
    }

    cameracapture()
    {
        this.logodialogRef = this.dialog.open(WebCameraComponent, {
            panelClass: 'fuse-web-camera',
            data      : {
                profile: this.team,
            },
            height: '600px',
            width: '300px',
        });

        this.logodialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }

                this.logoURL = response[0];
                this.selectedFiles = response[1];
            });
    }

    doccameracapture()
    {
        this.logodialogRef = this.dialog.open(WebCameraComponent, {
            panelClass: 'fuse-web-camera',
            data      : {
                profile: this.team,
            },
            height: '600px',
            width: '300px',
        });

        this.logodialogRef.afterClosed()
            .subscribe(response => {
                if ( !response )
                {
                    return;
                }

                this.pictureURL = response[0];
                this.selectedDocFiles = response[1];
            });
    }

    prevent(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
        }
    }
}

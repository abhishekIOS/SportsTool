import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamFormMenuComponent } from './team-form-menu.component';

describe('TeamFormMenuComponent', () => {
  let component: TeamFormMenuComponent;
  let fixture: ComponentFixture<TeamFormMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamFormMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamFormMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CreateScheduleComponent } from './create-schedule/create-schedule.component';
import { SharedModule } from '../../core/modules/shared.module';


const routes = [
  {
    path: '',
    component: CreateScheduleComponent
  }
];

@NgModule({
  imports: [
    CommonModule, 
    SharedModule,
    RouterModule.forChild(routes)],
  declarations: [CreateScheduleComponent]
})
export class CreateScheduleModule {}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgForm } from '@angular/forms';

import {
  AngularFirestore,
  AngularFirestoreDocument
} from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';

import 'moment';
declare let moment: any;

@Component({
  selector: 'app-user',
  templateUrl: './create-schedule.component.html',
  styleUrls: ['./create-schedule.component.scss']
})
export class CreateScheduleComponent implements OnInit {
  form: FormGroup;
  formErrors: any;

  private itemDoc: AngularFirestoreDocument<any>;
  item: Observable<any>;
  user = {
    firstName: '',
    lastName: '',
    nickName: '',
    gender: '',
    birthday: '',
    cityOfBirth: '',
    countryOfBirth: '',
    age: 0,
    sports: '',
    email: '',
    cellPhone: '',
    homePhone: '',
    role: '',
    address: '',
    address2: '',
    city: '',
    state: '',
    postalCode: ''
  };
  //   constructor() {}
  //   ngOnInit() {}

  profile(info) {
    console.log(info);
  }
  constructor(private formBuilder: FormBuilder, private afs: AngularFirestore) {
    // Reactive form errors
    this.formErrors = {
      firstName: {},
      lastName: {},
      nickName: {},
      gender: {},
      birthday: {},
      cityOfBirth: {},
      countryOfBirth: {},
      sports: {},
      email: {},
      cellPhone: {},
      homePhone: {},
      role: {},
      address: {},
      address2: {},
      city: {},
      state: {},
      postalCode: {},
      country: {},
      adaptiveNeeds: {}
    };

    // this will subscribe and console.log all the users in firestore upon
    this.afs
      .collection<any>('users')
      .valueChanges();
  }

  ngOnInit() {
    // Reactive Form
    this.form = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      nickName: [''],
      gender: ['', Validators.required],
      birthday: ['', Validators.required],
      cityOfBirth: [''],
      countryOfBirth: [''],
      sports: [''],
      email: ['', Validators.required],
      cellPhone: ['', Validators.required],
      homePhone: [''],
      role: ['', Validators.required],
      address: ['', Validators.required],
      address2: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      postalCode: ['', [Validators.required, Validators.maxLength(5)]],
      country: ['', Validators.required],
      adaptiveNeeds: ['']
    });
    let date = new Date();
    this.form.valueChanges.subscribe(() => {
      this.onFormValuesChanged();
      if (this.form.value.birthday) {
        let bday = moment(this.form.value.birthday);
        this.user.age = bday;
      }
    });
  }

  saveFB() {
    // taking the form value and putting it into profile. This makes it more managable.
    let profile = this.form.value;
    // this creates an ID with this format: '18Jan28T220328-z1i9ym5sqq'
    profile.id =
      moment().format('YYMMMDDTHHmmss') +
      '-' +
      Math.random()
        .toString(36)
        .slice(2);
    // below let you set your own ID
    this.afs.doc<any>(`users/${profile.id}`).set(profile);
    // below let firestore generate ID
    this.afs.collection<any>(`users`).add(profile);
  }

  deleteFB() {
    let profile = this.form.value;
    profile.id = '123456';
    // below lets you delete a document. You need to provide the ID.
    this.afs.doc<any>(`users/${profile.id}`).delete();
  }

  onFormValuesChanged() {
    for (const field in this.formErrors) {
      if (!this.formErrors.hasOwnProperty(field)) {
        continue;
      }

      // Clear previous errors
      this.formErrors[field] = {};

      // Get the control
      const control = this.form.get(field);

      if (control && control.dirty && !control.valid) {
        this.formErrors[field] = control.errors;
      }
    }
  }
}

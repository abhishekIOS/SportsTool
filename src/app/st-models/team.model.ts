import {Adaptive} from './adaptive.model';
export class SportColor {
    shirt: string = null;
    stripe: string = null;
    shorts: string = null;
}

export class Age {
    level: string = null;
    old: string = null;
}

export class Team {
    id: string = null;
    owner_id: string = null;
    club_id: string = null;
    name: string = null;
    age: Age = new Age();
    homecolor: SportColor = new SportColor();
    awaycolor: SportColor = new SportColor();
    gender: string = null;
    size: string = null;
    sports: any = [];
    emails: any = [];
    phones: any = [];
    urls: any = [];
    socialaccounts: any = [];
    date_create: Date;
    date_update: Date;
    logo: any = null;
    picture: any = null;
    description: any = null;
    adaptive: Adaptive = new Adaptive();
}

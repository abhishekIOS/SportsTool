import {Adaptive} from './adaptive.model';
export class Facility {
    id: string = null;
    name: string = null;
    owner_id: string = null;
    sports: any = [];
    emails: any = [];
    phones: any = [];
    urls: any = [];
    socialaccounts: any = [];
    streetaddress: string = null;
    city: string = null;
    state: string = null;
    country: string = null;
    adaptive: Adaptive = new Adaptive();
    postalCode: string = null;
    logo: any = null;
    description: any = null;
    date_create: Date;
    date_update: Date;
}

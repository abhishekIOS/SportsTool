import {Adaptive} from './adaptive.model';
export class Club {
    id: string = null;
    name: string = null;
    owner_id: string = null;
    sports: any = [];
    emails: any = [];
    phones: any = [];
    urls: any = [];
    socialaccounts: any = [];
    streetaddress: string = null;
    city: string = null;
    state: string = null;
    country: string = null;
    postalCode: string = null;
    logo: any = null;
    description: any = null;
    adaptive: Adaptive = new Adaptive();
    date_create: Date;
    date_update: Date;
}

export class LocationInfo {
    state: string;
    city: string;
    country: string;
    countryCode: string;
}
